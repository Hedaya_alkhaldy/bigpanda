@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                @if ($from && $to)
                <a href="{{ route('expenses.search',['to'=> $to,'from'=>$from]) }}" >Expense</a>

                @else
                <a href="{{ route('expenses.search') }}" >Expense</a>

                @endif

            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                @if ($from && $to)
                                <a href="{{ route('expenses.search',['to'=> $to,'from'=>$from]) }}" class="btn btn-light">Back</a>
                                @else
                                <a href="{{ route('expenses.index') }}" class="btn btn-light">Back</a>
                                @endif
                             </div>
                             <div class="card-body">
                                 @include('expenses.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
