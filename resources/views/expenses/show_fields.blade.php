<!-- Reason Field -->
<div class="form-group col-sm-6">
        {!! Form::label('reason', 'Reason:') !!}
    <p>{{ $expense->reason }}</p>
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
        {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $expense->amount }}</p>
</div>

<!-- Expense Date Field -->
<div class="form-group col-sm-6">
        {!! Form::label('expense_date', 'Expense Date:') !!}
    <p>{{ $expense->expense_date }}</p>
</div>

<!-- Notes Field -->
<div class="form-group col-sm-6">
        {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $expense->notes }}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
        {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $expense->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
        {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $expense->updated_at }}</p>
</div>

