<div class="table-responsive-sm">
    <table class="table table-striped" id="expenses-table">
        <thead>
            <tr>
                <th>Reason</th>
        <th>Amount</th>
        <th>Expense Date</th>
        <th>Notes</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>

            @if ($expenses )
            @foreach($expenses as $expense)
            <tr>
                <td>{{ $expense->reason }}</td>
            <td>{{ $expense->amount }}</td>
            <td>{{ $expense->expense_date }}</td>
            <td>{{ $expense->notes }}</td>
                <td>
                    {!! Form::open(['route' => ['expenses.destroy', ['to'=>$to,'from'=>$from,$expense->id]], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('expenses.show', ['to'=>$to,'from'=>$from,$expense->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('expenses.edit', ['to'=>$to,'from'=>$from,$expense->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
            @else
            <tr>
            <h6> no any expenses in this date</h6>
            </tr>
            @endif

        </tbody>
    </table>
</div>
