@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Expenses</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            Expenses
                            <a class="pull-right" href="{{ route('expenses.create') }}"><i
                                    class="fa fa-plus-square fa-lg"></i></a>
                        </div>


                        <div class="card-body">
                            {!! Form::open(['route' => 'expenses.search']) !!}

                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::label('from', 'From:') !!}
                                    {!! Form::date('from', null, ['class' => 'form-control','required']) !!}
                                </div>
                                <!-- Balance Field -->
                                <div class="col">
                                    {!! Form::label('to', 'To:') !!}
                                    {!! Form::date('to', null, ['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>

                            <div class="pull-right mr-3">
                                {!! Form::button('Done', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                                <a href="{{ route('dashboard') }}" class="btn btn-default">Cancel</a>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
