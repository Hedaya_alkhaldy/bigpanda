




</HEAD>
<BODY>
    <div class="form-group col-sm-3">
        {!! Form::label('expense_date', 'Date:') !!}
        {!! Form::date('expense_date', $date, ['class' => 'form-control','id'=>'reservation_date',]) !!}
    </div>
    <div class="form-group col-sm-3">
        {!! Form::date('to', $to, ['class' => 'form-control','id'=>'reservation_date','hidden']) !!}
    </div>
    <div class="form-group col-sm-3">
        {!! Form::date('from', $from, ['class' => 'form-control','id'=>'reservation_date','hidden']) !!}
    </div>

    @if ($expense)

    <table class="table table-striped" id="dataTable">
        <thead>
            <tr>
                <th class="text-center">The Reasons</th>
                <th class="text-center">Amount</th>
                <th class="text-center">Notes</th>
                <th>
                    <button type="button"  class='btn btn-ghost-success'  onclick='addRow()' style='width:50px;'>
                        <i class="fa fa-plus"></i></button>
                    </th>

            </tr>
            </thead>
    <tbody>


        <tr id='row_1'>
            <p>
                <td>
                    <div class="form-group col-sm-12">
                        <input list="item_id" name="reasons[]" value="{{$expense->reason  }}"  class="form-control form-select" style="width:100%;"  />

                    </div>
                </td>


                <td>
                    <div class="form-group col-sm-12">
                        <input type="text" required="required" value="{{$expense->amount  }}" class="form-control" id = 'amount[row_1]' name="amount[]" oninput="calculate()" onChange="updateCalculate(1)"  step="0.01" min=0>
                    </div>
                </td>
                <td>
                    <div class="form-group col-sm-12">
                    <input type="text" required="required" value="{{ $expense->notes }}" class="form-control lst" id ='total[row_1]' name="notes[]"  >
                    </div>
                </td>
            </p>
        </tr>
    </tbody>
</table>

<div class="form-group form-inline col-sm-3 float-right">
    <input type="text" hidden readonly class="form-control ml-4 " id ='allQuantity' name="allQyantity">
</br>
    <h6 class="pt-3"><b>Total Balance :</b></h6>
    <input type="text" readonly class="form-control pt-3 ml-2" id ='allTotal' name="allTotal"  value="{{ $expense->amount }}">
    </div>


    @elseif($expense==null)
    <table class="table table-striped" id="dataTable">
        <thead>
            <tr>
                <th class="text-center">The Reasons</th>
                <th class="text-center">Amount</th>
                <th class="text-center">Notes</th>
                <th>
                <button type="button"  class="btn btn-ghost-danger"   style="width:50px;" onclick="deleteRow()"><i class="fas fa-minus"></i></button>
                </th>
            </tr>
            </thead>
    <tbody>


        <tr id='row_1'>
            <p>
                <td>
                    <div class="form-group col-sm-12">
                        <input list="item_id" name="reasons[]" class="form-control form-select" style="width:100%;"  />

                    </div>
                </td>


                <td>
                    <div class="form-group col-sm-12">
                        <input type="text" required="required" class="form-control" id = 'amount[row_1]' name="amount[]" oninput="calculate()" onChange="updateCalculate(1)"  step="0.01" min=0>
                    </div>
                </td>
                <td>
                    <div class="form-group col-sm-12">
                    <input type="text" required="required" class="form-control lst" id ='total[row_1]' name="notes[]"  >
                    </div>
                </td>
            </p>
        </tr>
    </tbody>
</table>

<div class="form-group form-inline col-sm-3 float-right">
    <input type="text" hidden readonly class="form-control ml-4 " id ='allQuantity' name="allQyantity">
</br>
    <h6 class="pt-3"><b>Total Balance :</b></h6>
    <input type="text" readonly class="form-control pt-3 ml-2" id ='allTotal' name="allTotal">
    </div>

    @endif




<!-- Submit Field -->
<div class="form-group col-sm-12 pt-3">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if ($from && $to)
    <a href="{{ route('expenses.search',['to'=> $to,'from'=>$from]) }}" class="btn btn-secondary">Cancel</a>

    @else
    <a href="{{ route('expenses.index') }}" class="btn btn-secondary">Cancel</a>

    @endif
</div>


<SCRIPT language="javascript">
    var myTotal =0;
    var myQuantity =0;
    var row=0;
    var total=0;

    function calculate() {
        var rowCount = $('#dataTable tr').length-1;
        var myBox1 = document.getElementById('amount[row_'+rowCount+']').value;
        var myResult1 = myBox1  ;
        totalAll(myResult1);


    }

    function totalAll(total) {

        myTotal = Number(myTotal) + Number(total);
        var allTotal = document.getElementById('allTotal');
        allTotal.value = myTotal;


    }



    </script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script language="javascript">


var row=0;
var total=0;
$(document).on('keyup', '.lst', function(e) {
    var rowCount = $('#dataTable tr').length;
  var code = (e.keyCode ? e.keyCode : e.which);
  row=rowCount-1;
  var total = document.getElementById('total[row_'+row+']').value;

  if (code == 13 && total && total !=0) {
    var table = document.getElementById("dataTable");
    var row = table.insertRow(rowCount);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var qty = '[row_'.concat(rowCount,']');
    var amount = '[row_'.concat(rowCount,']');
    var total = '[row_'.concat(rowCount,']');
    var abc = 'this is text'; <?php $abc = "<script>document.write(rowCount)</script>"?>
    cell1.innerHTML='<div class="form-group col-sm-12"><input list="item_id" name="reasons[]" class="form-control form-select"  style="width:100%; height: 35px;" /></div>';
    cell2.innerHTML = '<div class="form-group col-sm-12"><input type="text" class="form-control"  name="amount[]"  id="amount[row_'+rowCount+']" oninput="calculate()"  onChange="updateCalculate('+rowCount+')"  step="0.01" min=0 required></div>';
    cell3.innerHTML = '<div class="form-group col-sm-12"><input type="text"   class="form-control lst" name="notes[]" id="total[row_'+rowCount+']" ></div>';
    $(this).focus().select();
    rowCount++;
  }
});

$(document).on('keydown', '.inputs', function(e) {
  var code = (e.keyCode ? e.keyCode : e.which);
  if (code == 13) {
    var index = $('.inputs').index(this) + 1;
    $('.inputs').eq(index).focus();
  }
});

function addRow()
{
    var rowCount = $('#dataTable tr').length;
    row=rowCount-1;
    var table = document.getElementById("dataTable");
    var row = table.insertRow(rowCount);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var qty = '[row_'.concat(rowCount,']');
    var amount = '[row_'.concat(rowCount,']');
    var total = '[row_'.concat(rowCount,']');
    var abc = 'this is text'; <?php $abc = "<script>document.write(rowCount)</script>"?>
    cell1.innerHTML='<div class="form-group col-sm-12"><input list="item_id" name="reasons[]" class="form-control form-select"  style="width:100%; height: 35px;" /></div>';
    cell2.innerHTML = '<div class="form-group col-sm-12"><input type="text" class="form-control"  name="amount[]"  id="amount[row_'+rowCount+']" oninput="calculate()"  onChange="updateCalculate('+rowCount+')"  step="0.01" min=0 required></div>';
    cell3.innerHTML = '<div class="form-group col-sm-12"><input type="text"   class="form-control lst" name="notes[]" id="total[row_'+rowCount+']" ></div>';
    $(this).focus().select();
    rowCount++;
}


function deleteRow() {
    var rowCount = $('#dataTable tr').length;
    var row=rowCount-1;

    if(row > 1)
    {
        document.getElementById("dataTable").deleteRow(rowCount-1);
        var newRow=row-1;
        updateCalculate(newRow);
    }
    if(row==1)
    {
        alert("the minimum number of rows should be 1. ");
    }
}

function updateCalculate(newRow) {
    var row=newRow;
    var myBox2 = document.getElementById('amount[row_'+row+']').value;
    updateTotalAll();
}








function  updateTotalAll() {

var allTotal = document.getElementById('allTotal');
var rowCount = $('#dataTable tr').length-1;
var sum =0;
for (var i =1; i <=rowCount; i++) {
    var qty = document.getElementById('amount[row_'+i+']').value;
    sum = Number(sum)+ Number(qty) ;
    allTotal.value = Number(sum);

}
}
</script>
