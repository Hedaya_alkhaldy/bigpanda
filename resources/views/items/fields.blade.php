@if($formType =='createType' )
    <!-- Item Number Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('item_number', 'Item Number:') !!}
        {!! Form::text('item_number', $numberOfItem+1, ['class' => 'form-control']) !!}
    </div>

@endif

@if($formType =='editType')
    <!-- Item Number Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('item_number', 'Item Number:') !!}
        {!! Form::text('item_number', null, ['class' => 'form-control']) !!}
    </div>
@endif


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    {!! Form::number('sale_price', null, ['class' => 'form-control','step="0.001"' ,'min=0' ]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('items.index') }}" class="btn btn-secondary">Cancel</a>
</div>
