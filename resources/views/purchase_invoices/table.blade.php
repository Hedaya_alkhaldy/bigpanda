<div class="table-responsive-sm">
    <table class="table table-striped" id="purchaseInvoices-table">
        <thead>
        <tr>
        <th>#</th>
        <th>Date</th>
        <th>Invoice Id</th>
        <th>Provider Name</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Total</th>
        <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($purchaseInvoices as $key => $purchaseInvoice)
            <tr>
            <td>{{ $key+1 }}</td>
            <td><?php echo(date_format($purchaseInvoice->invoice_id, 'Y-m-d')); ?></td>
            <td>{{ $purchaseInvoice->invoice_id->format('dmy') }}</td>
            <td>{{ $purchaseInvoice->provider->name }}</td>
            <td>{{ $purchaseInvoice->type }}</td>
            <td>{{ $purchaseInvoice->quantity }}</td>
            <td>{{ $purchaseInvoice->total }}</td>
                <td>
                    {!! Form::open(['route' => ['purchaseInvoices.destroy', $purchaseInvoice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('purchaseInvoices.show', [$purchaseInvoice->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('purchaseInvoices.edit', [$purchaseInvoice->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
