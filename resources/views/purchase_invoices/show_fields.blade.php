<!-- Id Field -->
<div class="row">
<div class="col-sm-6">
    {!! Form::label('Id', 'ID:') !!}
    <p>{{ $purchaseInvoice->id }}</p>
</div>

<!-- Date Field -->
<div class="col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    <p><?php echo(date_format($purchaseInvoice->invoice_id, 'Y-m-d')); ?></p>
</div>

<!-- Provider Id Field -->
<div class="col-sm-6">
    {!! Form::label('provider_name', 'Provider Name:') !!}
    <p>{{ $purchaseInvoice->provider->name }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $purchaseInvoice->type }}</p>
</div>
<!-- quantity Field -->
<div class="col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{{ $purchaseInvoice->quantity }}</p>
</div>
<!-- total Field -->
<div class="col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $purchaseInvoice->total }}</p>
</div>
<!-- Created At Field -->
<div class="col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $purchaseInvoice->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $purchaseInvoice->updated_at }}</p>
</div>

<div class="table-responsive col-sm-9">
    <table class="table">
        <thead>
        <tr>
        <th>Item Name</th>
        <th>Quantity</th>
        <th>Price/pcs</th>
        <th>Total</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                @foreach($purchaseInvoice->items as $item)
                <td>{{$item->name }}</td>
                <td>{{$item->pivot['quantity']}}</td>
                <td>{{$item->pivot['purchasing_price']}}</td>
                <td>{{$item->pivot['total']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>

</div>
