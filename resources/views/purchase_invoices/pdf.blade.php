<!DOCTYPE html>
<html>
<head>
    <title>Purchase Invoices</title>
</head>
<style>
    table, td, th {
      border: 1px solid #ddd;
      text-align: left;
      font-size: smaller;    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    </style>
<body>

    <div class="table-responsive">
        <h1>2022</h1>
        <h1>Purchase Invoices </h1>
            <table border="5px" bordercolor="#8707B0">
                <thead>
                    <tr>
                    <th>Invoice Number</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Provider Name</th>
                    <th class="text-center">Purchase Details</th>
                    </tr>
                    </thead>
                <tbody>
                    @foreach($purchaseInvoices as $purchaseInvoice)
                    <tr>
                    <td>{{ $purchaseInvoice->id }}</td>
                    <td><?php echo(date_format($purchaseInvoice->invoice_id, 'Y-m-d')); ?></td>
                    <td>{{ $purchaseInvoice->type }}</td>
                    <td>{{ $purchaseInvoice->provider->name }}</td>
                    <td>
            <table border="1px" bordercolor="#F35557">

            <thead>
                <tr>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price/pcs</th>
                <th>Total</th>
                </tr>
                </thead>
                @foreach($purchaseInvoice->items as $purchaseItem)
                <tr>
                    @foreach($items as $item)
                    @if($purchaseItem->id == $item->id )
                    <td class="text-xs">{{ $item->name }}  </td>




                <td class="text-xs">  {{$purchaseItem->pivot['quantity']}}</td>
                <td class="text-xs">  {{$purchaseItem->pivot['purchasing_price']}}</td>
                <td class="text-xs">  {{$purchaseItem->pivot['total']}}</td>
                @endif

                @endforeach

            </tr>

            @endforeach
            <p style="text-align:right;"><b>Total Balance: {{ $purchaseInvoice->total }} JD</b></p>
        </table>
    </td>

    </tr>
       @endforeach
</tbody>
</table>
</div>

</body>
</html>
