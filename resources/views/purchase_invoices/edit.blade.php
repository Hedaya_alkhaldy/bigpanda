@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('purchaseInvoices.index') !!}">Purchase Invoice</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
            @include('flash::message')
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Purchase Invoice</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($purchaseInvoice, ['route' => ['purchaseInvoices.update', $purchaseInvoice->id], 'method' => 'patch']) !!}

                              @include('purchase_invoices.update_fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
