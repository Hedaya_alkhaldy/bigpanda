
<!-- Provider Id Field -->

<div class="form-group col-sm-4">
    {!! Form::label('provider_id', 'Provider Name:') !!}

    {!! Form::select('provider_id' ,$providers , null, ['class' => 'form-control selectpicker' , 'style'=>"width: 200px;height: 50px;" , 'id'=>'select' , 'data-live-search'=>'true', 'data-show-subtext'=>'true']) !!}
</div>
<!-- Type Field -->
<div class="form-group col-sm-4">
    {!! Form::label('type', 'Invoice Type:') !!}
    <br>
    {{ Form::radio('type', 'Cash',['class'=>' form-control' ]) }}
    {!! Form::label('Cash', 'Cash') !!}
    {{ Form::radio('type', 'Credit', ['class'=>' form-control']) }}
    {!! Form::label('Credit', 'Credit') !!}
</div>
<table class="table table-striped" id="dataTable">
    <thead>
        <tr>
            <th>Item Name</th>
            <th>Quantity</th>
            <th>Price/pices</th>
            <th>Total</th>
            <th>
                <button type="button"  class='btn btn-ghost-success'  onclick='addRow()' style='width:50px;'>
                <i class="fa fa-plus"></i></button>
            </th>
        </tr>
        </thead>
<tbody>
<?php
$qty = 0;
$price = 0;
$total = 0; ?>
@foreach ($purchaseInvoice->items as  $key=>$invoiceItem )
<?php
$qty = 'qty[row_'.$key.']';
$price = 'price[row_'.$key.']';
$total = 'total[row_'.$key.']';
?>

<tr id='row_1'>
    <p>
        <td>

            <select name='item_id[]' class="form-control" style="width:200px; height: 35px;" readonly>
                <option value="{{$invoiceItem->id }}" >{{$invoiceItem->name}}</option>
            </select>
            <input type="hidden" value="{{ $invoiceItem->pivot['id'] }}" name="pivotId[]">

        </td>
        <td>
            <div class="form-group col-sm-12">
            <input type="number" class="form-control"  value={{ $invoiceItem->pivot['quantity'] }} required="required" id='{{ $qty }}' name="qty[]" onChange="updateCalculate({{$key}})" min="0">
            </div>
        </td>
        <td>
            <div class="form-group col-sm-12">
            <input type="number" required="required"  value={{$invoiceItem->pivot['purchasing_price']}} class="form-control" id='{{ $price }}' name="price[]" onChange="updateCalculate({{$key}})" step="0.01" min=0>
            </div>
        </td>
        <td>
            <div class="form-group col-sm-12">
            <input type="text" required="required" value={{$invoiceItem->pivot['total']}} class="form-control lst" id ='{{ $total }}' name="total[]" readonly>
            </div>
        </td>
        <td>
            <div class="text-xs">
                <a href="{{ route('purchase.item.destroy', ['purchaseInvoiceId' => $purchaseInvoice->id , 'item_id' => $invoiceItem->id  , 'pivotId'=>$invoiceItem->pivot['id']]) }}"
                    class ='btn btn-ghost-danger'>
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </td>
    </p>
</tr>

@endforeach

</tbody>
</table>
<div class="form-group form-inline col-sm-5 float-right">
    {{-- <label ><b>All Qyantity : </b> </label> --}}
    <input type="hidden" readonly class="form-control ml-3 " value={{$purchaseInvoice->quantity}} id ='allQuantity' name="allQuantity">
</br>
    <h6 class="pt-3"><b>Total Balance :</b></h6>
    <input type="text" readonly class="form-control pt-3 ml-2" value={{$purchaseInvoice->total}} id ='allTotal' name="allTotal">
    </div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('purchaseInvoices.index') }}" class="btn btn-secondary">Cancel</a>
</div>

<script language="javascript">
function calculate() {
var rowCount = $('#dataTable tr').length-1;
var myBox1 = document.getElementById('qty[row_'+rowCount+']').value;
var myBox2 = document.getElementById('price[row_'+rowCount+']').value;
var total = document.getElementById('total[row_'+rowCount+']');
var myResult1 = myBox1 * myBox2 ;
total.value = myResult1;
totalAll(total);
if(myBox2){
    quantityAll(myBox1);

}




}

function totalAll(total) {

myTotal = Number(myTotal) + Number(total.value);
var allTotal = document.getElementById('allTotal');
allTotal.value = Number(allTotal.value) + Number(myTotal);

}

function quantityAll(qty) {

var allQty = document.getElementById('allQuantity');
allQty.value = Number(allQty.value) + Number(qty);



}

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script language="javascript">
var myTotal =0;
var myQuantity =0;
var row=0;
var total=0;

function addRow()
{
var rowCount = $('#dataTable tr').length-1;
var table = document.getElementById("dataTable");
var row = table.insertRow(rowCount+1);
var cell1 = row.insertCell(0);
var cell2 = row.insertCell(1);
var cell3 = row.insertCell(2);
var cell4 = row.insertCell(3);
var qty = '[row_'.concat(rowCount,']');
var price = '[row_'.concat(rowCount,']');
var total = '[row_'.concat(rowCount,']');
var abc = 'this is text'; <?php $abc = "<script>document.write(rowCount)</script>"?>
cell1.innerHTML='<input list="item_id" name="newItemId[]" class="form-control form-select" placeholder="search item" style="width:200px;  height: 35px;" />';
cell1.innerHTML+='<datalist id="item_id" >@foreach ( $items as $item )<option  value="{{ $item['item_number'] }}">{{ $item['name'] }}</option>@endforeach </datalist>';
cell2.innerHTML = '<div class="form-group col-sm-12"><input type="number" class="form-control"  name="newQty[]"  id="qty[row_'+rowCount+']" oninput="calculate()"  onChange="updateCalculate('+rowCount+')" min="0" required></div>';
cell3.innerHTML = '<div class="form-group col-sm-12"><input type="number" class="form-control"  name="newPrice[]"  id="price[row_'+rowCount+']" oninput="calculate()" onChange="updateCalculate('+rowCount+')" step="0.01" min=0  required></div>';
cell4.innerHTML = '<div class="form-group col-sm-12"><input type="text"   class="form-control " name="newTotal[]" id="total[row_'+rowCount+']" readonly></div>';

}

function deleteRow() {
    var rowCount = $('#dataTable tr').length;
    var row=rowCount-1;

    if(row > 1)
    {
        document.getElementById("dataTable").deleteRow(rowCount-1);
    }
    if(row==1)
    {
        alert("the minimum number of rows should be 1. ");
    }
}


function updateCalculate(id) {
var rowCount = id;
var myBox1 = document.getElementById('qty[row_'+rowCount+']').value;
var myBox2 = document.getElementById('price[row_'+rowCount+']').value;
var total = document.getElementById('total[row_'+rowCount+']');
var myResult1 = myBox1 * myBox2 ;
total.value = myResult1;
//updateQuantityAll();
updateTotalAll();


}

function updateQuantityAll() {

var allQty = document.getElementById('allQuantity');

var rowCount = $('#dataTable tr').length-1;
var sum =0;
allQty.value=0;
for (var i =0; i < rowCount; i++) {
    var qty = document.getElementById('qty[row_'+i+']').value;
    sum = Number(sum)+ Number(qty) ;
    allQty.value = Number(sum);
}

}

function  updateTotalAll() {

var allTotal = document.getElementById('allTotal');
var rowCount = $('#dataTable tr').length-1;
var sum =0;
allTotal.value=0;
for (var i =0; i <rowCount; i++) {
    var qty = document.getElementById('total[row_'+i+']').value;
    sum = Number(sum)+ Number(qty) ;
    allTotal.value = Number(sum);

}


}

</script>

