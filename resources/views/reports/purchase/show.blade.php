@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('reports.purchases.index') }}">Purchase Report</a>
    </li>
    <li class="breadcrumb-item active">Show</li>
</ol>

    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                            <strong>Details</strong>
                            <a href="{{ route('reports.purchases.index') }}" class="btn btn-light">Back</a>
                             <div class='btn-group float-right'>
                                <a href="{{ route('purchase.report.pdf', [ 'from' => $from, 'to' => $to]) }}" class='btn btn-ghost-danger'><i class="fa fa-file-pdf-o"></i> Export as PDF</a>
                                <a href="{{ route('purchase.report.excel', [ 'from' => $from, 'to' => $to]) }}" class='btn btn-ghost-success'><i class="fa fa-file-excel-o"></i> Export as Excel</a>
                            </div>

                         </div>
                         <div class="card-body">
                             @include('reports.purchase.table')
                              <div class="pull-right mr-3">

                              </div>


                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

