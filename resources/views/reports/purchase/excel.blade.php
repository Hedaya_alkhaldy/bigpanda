<!DOCTYPE html>
<html>
<head>
    <title>Purchase Invoices</title>
</head>
<style>
    table, td, th {
      border: 1px solid #ddd;
      text-align: center;
      font-size: smaller;    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    </style>
<body>

    <div class="table-responsive">
        <h2>Purchase Invoices </h2>
        <h5>From: {{$from}} To: {{$to}}</h5>
            <table border="5px" bordercolor="#8707B0">
                <thead>
                    <tr>
                    <th><b>Invoice Number</b></th>
                    <th><b>Date</b></th>
                    <th><b>Type</b></th>
                    <th><b>Provider Name</b></th>
                    <th><b>Purchase Details</b></th>
                    </tr>
                    </thead>
                <tbody>
                    @foreach($purchaseInvoices as $purchaseInvoice)
                    <tr>
                    <td>{{ $purchaseInvoice->id }}</td>
                    <td><?php echo(date_format($purchaseInvoice->invoice_id, 'Y-m-d')); ?></td>
                    <td>{{ $purchaseInvoice->type }}</td>
                    <td>{{ $purchaseInvoice->provider->name }}</td>
                    <td></td>
                    <td>
                        <table border="1px" bordercolor="#F35557">
                        <thead>
                            <tr>
                            <th style="font-weight: 500">Name</th>
                            <th style="font-weight: 500">Quantity</th>
                            <th style="font-weight: 500">Price/pcs</th>
                            <th style="font-weight: 500">Total</th>
                            </tr>
                            </thead>
                            @foreach($purchaseInvoice->items as $purchaseItem)
                                <tr>
                                    @foreach($items as $item)
                                        @if($purchaseItem->id == $item->id )
                                            <td class="text-xs">{{ $item->name }}  </td>
                                            <td class="text-xs">  {{$purchaseItem->pivot['quantity']}}</td>
                                            <td class="text-xs">  {{$purchaseItem->pivot['purchasing_price']}}</td>
                                            <td class="text-xs">  {{$purchaseItem->pivot['total']}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endforeach
                            <tr></tr>
                        <tr style="text-align:right;"><td></td><td></td><td><b>Total Balance:</b></td><td class="text-xs">{{ $purchaseInvoice->total }} JD</td></tr>
                    </table>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>
