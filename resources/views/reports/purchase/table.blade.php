<div class="table-responsive-sm">
    <table class="table table-striped" id="purchaseInvoices-table">
        <div class="table-responsive">
            <h5>From: {{$from}} To: {{$to}}</h5>
            <table class="table table-striped" id="dataTable">
                <thead>
                    <tr>
                    <th>Invoice Number</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Provider Name</th>
                    <th class="text-center">Purchase Details</th>
                    </tr>
                    </thead>
                <tbody>
                    @foreach($purchaseInvoices as $purchaseInvoice)
                    <tr>
                    <td>{{ $purchaseInvoice->id }}</td>
                    <td><?php echo(date_format($purchaseInvoice->invoice_id, 'Y-m-d')); ?></td>
                    <td>{{ $purchaseInvoice->type }}</td>
                    <td>{{ $purchaseInvoice->provider->name }}</td>
                    <td>
                        <table class="table table-striped" id="dataTable">
                            <thead>
                                <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Price/pcs</th>
                                <th>Total</th>
                                </tr>
                                </thead>
                                @foreach($purchaseInvoice->items as $purchaseItem)
                                <tr>
                                    @foreach($items as $item)
                                        @if($purchaseItem->id == $item->id )
                                            <td class="text-xs">{{ $item->name }}  </td>
                                            <td class="text-xs">  {{$purchaseItem->pivot['quantity']}}</td>
                                            <td class="text-xs">  {{$purchaseItem->pivot['purchasing_price']}}</td>
                                            <td td class="text-xs">  {{$purchaseItem->pivot['total']}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                                @endforeach
                        </table>
                        <p style="text-align:right;"><b>Total Balance: {{ $purchaseInvoice->total }} JD</b></p>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
