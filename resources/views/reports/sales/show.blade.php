@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('reports.sales.index') }}">Sales Report</a>
            </li>
            <li class="breadcrumb-item active">all</li>
     </ol>

     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('reports.sales.index') }}" class="btn btn-light">Back</a>
                                  <div class='btn-group float-right'>
                                    <a href="{{ route('all.sales.pdf', [ 'to'=>$to,'from'=>$from]) }}" class='btn btn-ghost-danger'><i class="fa fa-file-pdf-o"></i> Export as PDF</a>
                                    <a href="{{ route('all.sales.excel', [ 'to'=>$to,'from'=>$from]) }}" class='btn btn-ghost-success'><i class="fa fa-file-excel-o"></i> Export as Excel</a>

                                </div>
                                </div>
                             <div class="card-body">
                                 @include('reports.sales.table')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
