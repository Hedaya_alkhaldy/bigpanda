<!DOCTYPE html>
<html>
<head>
    <title>Sales Invoices</title>
</head>
<style>
    table, td, th {
      border: 1px solid;
      text-align: center;
      font-size: smaller;    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    </style>
    <body>

<div class="table-responsive-sm">
    <h1>2022</h1>
    <h1>Sales Invoices </h1>
    <h5>From {{ $from }}  To {{ $to }}</h5>
    <table border="5px" bordercolor="#8707B0">
        <tr>
            <th>#</th>
            <th>Invoice Id</th>
            <th>Sales Detail</th>
        </tr>
        @foreach($salesInvoices as $key => $salesInvoice)

        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $salesInvoice->invoice_id->format('dmy') }}</td>
        <td>
        <table border="5px" bordercolor="#F35557">
        <tr>
            <th>Item Name</th>
            <th>Quantity</th>
            <th>Price/pcs</th>
            <th>Total</th>
        </tr>
        @foreach($salesInvoice->items as $salesItem)

        <tr>
            @foreach($items as $item)
            @if($salesItem->id == $item->id )
        <td class="text-xs">{{ $item->name }}  </td>
        <td class="text-xs">  {{$salesItem->pivot['quantity']}}</td>
        <td class="text-xs">  {{$salesItem->pivot['sale_price']}}</td>
        <td class="text-xs">  {{$salesItem->pivot['total']}}</td>
        @endif
        @endforeach
        </tr>
        @endforeach
        </table>
        <p class ='pt-3' style="text-align:right;"><b>Total Balance: {{ $salesInvoice->total }} JD</b></p>

        </td>
        @endforeach
        </tr>
        </table>

</div>

</body>
</html>
