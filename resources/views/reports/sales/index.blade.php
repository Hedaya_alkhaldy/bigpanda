@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Purchase Invoices</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Sales Report
                         </div>
                         <div class="card-body">
                            
            {!! Form::open(['route' => 'sales.report.show']) !!}

<div class="card-body">
    <div class="row">
        <div class="form-group col-sm-6">
            {!! Form::label('from', 'From:') !!}
            {!! Form::date('from', null, ['class' => 'form-control']) !!}
        </div>
        <!-- Balance Field -->
        <div class="col">
            {!! Form::label('to', 'To:') !!}
            {!! Form::date('to', null, ['class' => 'form-control','step=0.01']) !!}
        </div>
    </div>


</div>

                              <div class="pull-right mr-3">
                                {!! Form::button('Done', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                                <a href="{{ route('dashboard') }}" class="btn btn-default">Cancel</a>
                              </div>

                              {!! Form::close() !!}
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

