
<style>
    table, td, th {
      border: 1px  #fff;
      text-align: center;
     }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    </style>
<div class="table-responsive-sm">
    <table class="table" id="salesInvoices-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Invoice Id</th>
                <th colspan="3">Sales Detail</th>
            </tr>
        </thead>
        <tbody>
        @foreach($salesInvoices as $key => $salesInvoice)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $salesInvoice->invoice_id->format('dmy') }}</td>
               <td>
                <table  >
                    <thead>
                        <tr>
                        <th>Item Name</th>
                        <th>Quantity</th>
                        <th>Price/pcs</th>
                        <th>Total</th>
                        </tr>
                        </thead>
                        @foreach($salesInvoice->items as $salesItem)
                        <tr>
                            @foreach($items as $item)
                            @if($salesItem->id == $item->id )
                        <td class="text-xs">{{ $item->name }}  </td>
                        <td class="text-xs">  {{$salesItem->pivot['quantity']}}</td>
                        <td class="text-xs">  {{$salesItem->pivot['sale_price']}}</td>
                        <td class="text-xs">  {{$salesItem->pivot['total']}}</td>
                        @endif
                        @endforeach
                    </tr>
                    @endforeach
                </table>
                <p class ='pt-3' style="text-align:right;"><b>Total Balance: {{ $salesInvoice->total }} JD</b></p>

               </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
