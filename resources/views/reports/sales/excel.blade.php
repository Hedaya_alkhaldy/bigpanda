
<style>
    table, td, th {
      border: 1px  #fff;
      text-align: center;
     }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    </style>
<div class="table-responsive-sm">

    <h1>Sales Invoices </h1>
    <h5>From {{ $from }}  To {{ $to }}</h5>
    <table class="table pt-5" id="salesInvoices-table">
        <thead>
            <tr>
                <th><b>#</b></th>
                <th><b>Invoice Id</b></th>
                <th><b>Sales Detail</b></th>
            </tr>
        </thead>
        <tbody>
        @foreach($salesInvoices as $key => $salesInvoice)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $salesInvoice->invoice_id->format('dmy') }}</td>
               <td>
                <table  >
                    <thead>
                        <tr>
                        <th><b>Item Name</b></th>
                        <th><b>Quantity</b></th>
                        <th><b>Price/pcs</b></th>
                        <th><b>Total</b></th>
                        </tr>
                        </thead>
                        @foreach($salesInvoice->items as $salesItem)
                        <tr>
                            @foreach($items as $item)
                            @if($salesItem->id == $item->id )
                        <td class="text-xs">{{ $item->name }}  </td>
                        <td class="text-xs">  {{$salesItem->pivot['quantity']}}</td>
                        <td class="text-xs">  {{$salesItem->pivot['sale_price']}}</td>
                        <td class="text-xs">  {{$salesItem->pivot['total']}}</td>
                        @endif
                        @endforeach
                    </tr>
                   
                    @endforeach
                    <tr></tr>
            <tr style="text-align:right;"><td></td><td></td><td><b>Total Balance: </b></td><td>{{ $salesInvoice->total }} JD</td></tr>
       
                </table>
               </td>
            </tr>
             @endforeach
        </tbody>
    </table>
</div>
