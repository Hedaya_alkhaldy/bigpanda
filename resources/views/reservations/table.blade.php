<div class="table-responsive-sm">
    <table class="table table-striped" id="reservations-table">
        <thead>
            <tr>
                <th>Screen </th>
        <th>Reservation Time From</th>
        <th>Reservation Time To</th>
        <th>Reservation Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @if ($reservations == null )
            @foreach($reservations as $reservation)
            <tr>
                <td>{{ $reservation->screens->screen_name }}</td>
            <td>{{ $reservation->reservation_time_from }}</td>
            <td>{{ $reservation->reservation_time_to }}</td>
            <td>{{ $reservation->reservation_date }}</td>
                <td>
                    {!! Form::open(['route' => ['reservations.destroy', $reservation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reservations.show', [$reservation->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('reservations.edit', [$reservation->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

            @endif

        </tbody>
    </table>
</div>
