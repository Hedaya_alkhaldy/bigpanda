<!-- Screen Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('screen_id', 'Screen :') !!}
    {!! Form::select('screen_id' ,$screens , null, ['class' => 'form-control ' ,'style'=>"height:35px "  ]) !!}

    <!-- Reservation Time Form Field -->
<div class=" pt-3">
    {!! Form::label('reservation_time_from', 'Form:') !!}
    {!! Form::time('reservation_time_from', null, ['class' => 'form-control','id'=>'reservation_time_from']) !!}
</div>
</div>

<!-- Reservation Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_date', 'Reservation Date:') !!}
    {!! Form::date('reservation_date', $date, ['class' => 'form-control','id'=>'reservation_date',]) !!}
</div>


<!-- Reservation Time To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_time_to', 'To:') !!}
    {!! Form::time('reservation_time_to', null, ['class' => 'form-control','id'=>'reservation_time_to']) !!}
</div>




<table class="table table-striped" id="dataTable">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Price/pices</th>
                <th>Total</th>
                <th>
                <button type="button"  class="btn btn-ghost-danger"   style="width:50px;" onclick="deleteRow()"><i class="fas fa-minus"></i></button>

                </th>
            </tr>
            </thead>
    <tbody>

        <tr id='row_1'>
            <p>
                <td>
                    <div class="form-group col-sm-12">
                        <input list="item_id" name="item_id[]" class="form-control form-select" style="width:200px;" placeholder="search item" />
                        <datalist id="item_id" >
                            @foreach ( $items as $item )
                            <option  value="{{ $item['item_number'] }}">{{ $item['name'] }}</option>
                            @endforeach
                        </datalist>
                    </div>

                </td>
                <td>
                    <div class="form-group col-sm-12">
                    <input type="number" class="form-control"  id='qty[row_1]' name="qty[]" oninput="calculate()" onChange="updateCalculate(1)"  min="0">
                    </div>
                </td>
                <td>
                    <div class="form-group col-sm-12">
                        <input list="price_id" id = 'price[row_1]' name="price[]" oninput="calculate()" onChange="updateCalculate(1)" class="form-control form-select" style="width:200px;" placeholder="search item" />
                        <datalist id="price_id" >
                            @foreach ( $prices as $price )
                            <option  value="{{ $price['sale_price'] }}">{{ $price['sale_price'] }}</option>
                            @endforeach
                        </datalist>
                  </div>                    </div>
                </td>

                <td>
                    <div class="form-group col-sm-12">
                    <input type="text"  class="form-control lst " id ='total[row_1]' name="total[]" readonly>
                    </div>
                </td>
            </p>
        </tr>
    </tbody>
    </table>
    <div class="form-group form-inline col-sm-5 float-right">
        {{-- <label ><b>All Qyantity : </b> </label> --}}
        <input type="hidden" readonly class="form-control ml-12 " id ='allQuantity' name="allQuantity">
    </br>
        <h6 class="pt-3"><b>Total Balance :</b></h6>
        <input type="text" readonly class="form-control pt-3 ml-2" id ='allTotal' name="allTotal">
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('reservations.index') }}" class="btn btn-secondary">Cancel</a>
        </div>

<script language="javascript">
var myTotal =0;
var myQuantity =0;
var total=0;

function calculate() {
    var rowCount = $('#dataTable tr').length-1;
    var myBox1 = document.getElementById('qty[row_'+rowCount+']').value;
    var myBox2 = document.getElementById('price[row_'+rowCount+']').value;
    var total = document.getElementById('total[row_'+rowCount+']');
    var myResult1 = myBox1 * myBox2 ;
    total.value = myResult1;
    totalAll(total);
    if(myBox2){
        quantityAll(myBox1);
    }

}

function totalAll(total) {

    myTotal = Number(myTotal) + Number(total.value);
    var allTotal = document.getElementById('allTotal');
    allTotal.value = myTotal;



}
function getPrice(id,price) {

    var item_id = document.getElementById('item[row_'+id+']');
    var price_col = document.getElementById('price[row_'+id+']');
    price_col.value = price;





}

function quantityAll(qty) {

    myQuantity = Number(myQuantity) + Number(qty);
    var allQuantity = document.getElementById('allQuantity');
    allQuantity.value = myQuantity;



}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script language="javascript">

var myTotal =0;
var myQuantity =0;
var row=0;
var total=0;
$(document).on('keyup', '.lst', function(e) {
    var rowCount = $('#dataTable tr').length;
  var code = (e.keyCode ? e.keyCode : e.which);
  row=rowCount-1;
  var total = document.getElementById('total[row_'+row+']').value;

  if (code == 13 && total && total !=0) {
    var table = document.getElementById("dataTable");
    var row = table.insertRow(rowCount);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var qty = '[row_'.concat(rowCount,']');
    var price = '[row_'.concat(rowCount,']');
    var total = '[row_'.concat(rowCount,']');
    var abc = 'this is text'; <?php $abc = "<script>document.write(rowCount)</script>"?>
    cell1.innerHTML='<div class="form-group col-sm-12">@foreach ( $items as $item )<input list="item_id" id="item[row_'+rowCount+']" onchange="getPrice('+rowCount+',{{ $item['price'] }})" name="item_id[]" class="form-control form-select" placeholder="search item" style="width:200px;" />';
    cell1.innerHTML+='<datalist id="item_id" ><option  value="{{ $item['item_number'] }}">{{ $item['name'] }}</option>@endforeach </datalist></div>';
    cell2.innerHTML = '<div class="form-group col-sm-12"><input type="text" class="form-control"  name="qty[]"  id="qty[row_'+rowCount+']" oninput="calculate()" onChange="updateCalculate('+rowCount+')"  min="0"></div>';
    cell3.innerHTML='<div class="form-group col-sm-12">@foreach ( $prices as $price )<input oninput="calculate()" onChange="updateCalculate('+rowCount+')" list="price" id="price[row_'+rowCount+']" onchange="getPrice('+rowCount+',{{ $item['price'] }})" id="price[row_'+rowCount+']" name="price[]" class="form-control form-select" placeholder="search item" style="width:200px;" />';
     cell3.innerHTML+='<datalist id="price" ><option  value="{{ $price['sale_price'] }}">{{ $price['sale_price'] }}</option>@endforeach </datalist></div>';
    cell4.innerHTML = '<div class="form-group col-sm-12"><input type="text"   class="form-control lst" name="total[]" id="total[row_'+rowCount+']" readonly></div>';
    $(this).focus().select();
    rowCount++;
}
});

$(document).on('keydown', '.inputs', function(e) {
  var code = (e.keyCode ? e.keyCode : e.which);
  if (code == 13) {
    var index = $('.inputs').index(this) + 1;
    $('.inputs').eq(index).focus();
  }
});

function deleteRow() {
    var rowCount = $('#dataTable tr').length;
    var row=rowCount-1;

    if(row > 1)
    {
        document.getElementById("dataTable").deleteRow(rowCount-1);
        var newRow=row-1;
        updateCalculate(newRow);

    }
    if(row==1)
    {
        alert("the minimum number of rows should be 1. ");
    }
}

function updateCalculate(newRow) {
    var row=newRow;
    var myBox1 = document.getElementById('qty[row_'+row+']').value;
    var myBox2 = document.getElementById('price[row_'+row+']').value;
    var total = document.getElementById('total[row_'+row+']');
    var myResult1 = myBox1 * myBox2 ;
    total.value = myResult1;
    updateQuantityAll();
    updateTotalAll();
}

function updateQuantityAll() {

var allQty = document.getElementById('allQuantity');
var rowCount = $('#dataTable tr').length-1;
var sum =0;
for (var i =1; i <=rowCount; i++) {
    var qty = document.getElementById('qty[row_'+i+']').value;
    sum = Number(sum)+ Number(qty) ;
    allQty.value = Number(sum);
}


}

function  updateTotalAll() {

var allTotal = document.getElementById('allTotal');
var rowCount = $('#dataTable tr').length-1;
var sum =0;
for (var i =1; i <=rowCount; i++) {
    var qty = document.getElementById('total[row_'+i+']').value;
    sum = Number(sum)+ Number(qty) ;
    allTotal.value = Number(sum);

}
}

</script>


