<!-- Screen Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('screen_id', 'Screen :') !!}
    <p>{{ $reservations->screens->screen_name }}</p>
</div>

<!-- Reservation Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_date', 'Reservation Date:') !!}
    <p>{{ $reservations->reservation_date }}</p>
</div>

<!-- Reservation Time From Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_time_from', 'Reservation Time From:') !!}
    <p>{{ $reservations->reservation_time_from }}</p>
</div>

<!-- Reservation Time To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_time_to', 'Reservation Time To:') !!}
    <p>{{ $reservations->reservation_time_to }}</p>
</div>



<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $reservations->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $reservations->updated_at }}</p>
</div>



<!-- Updated At Field -->

 <table class="table table-striped" id="dataTable">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Price/pices</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
            </thead>
    <tbody>

        <?php
         $qty = 0;
        $price = 0;
          $total = 0; ?>
       @foreach ($salesInvoice->items as $key=> $saleitem )
       <?php $qty = 'qty[row_'.$key.']';
       $price = 'price[row_'.$key.']';
       $total = 'total[row_'.$key.']';

       ?>

        <tr id='row_1'>
            <p>
                <td>
                     <div class="form-group col-sm-12">
                        {{$saleitem->name}}
                                        </div>
                    </td>
                <td>
                    <div class="form-group col-sm-12">
                        {{ $saleitem->pivot['sale_price'] }}
                                        </div>
                </td>
                <td>
                    <div class="form-group col-sm-12">
                        {{ $saleitem->pivot['quantity'] }}
                    </div>
                </td>

                <td>
                    <div class="form-group col-sm-12">
                        {{ $saleitem->pivot['total'] }}
                </div>
                </td>
                <td>
                </td>
            </p>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="form-group float-right mr-5">
   <h6 class="pt-3 "><b>Total Balance :  {{ $salesInvoice->total }}  </b></h6>


</div>




<!-- Submit Field -->
<div class="form-group col-sm-12 pt-3">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('reservations.index') }}" class="btn btn-secondary">Cancel</a>
</div>


