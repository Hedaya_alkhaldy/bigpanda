<!DOCTYPE html>
<html>
<head>
    <title>Reservations Invoices</title>
</head>
<style>

    table, td, th {
      border: 1px solid #ddd;
      text-align: center;
      font-size: smaller;    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    .column {
        float: left;
        width: 50%;
        padding: 10px;
        height: 300px; /* Should be removed. Only for demonstration */
      }

      /* Clear floats after the columns */
      .row:after {
        content: "";
        display: table;
        clear: both;
      }
      .border-none{
          border: none !important;
      }
    </style>
<body>

    <div class="table-responsive">
        <h1>2022</h1>
        <h1>Reservations Invoices </h1>

        <table style="border: none;">


                <tr style="border: none;">
                <td class="text-xs" style="border: none;"> Screen : {{$reservations->screens->screen_name}} </td>
                <td class="text-xs" style="border: none;"> Reservation Date:   {{ $reservations->reservation_date  }}</td>
                </tr>
                <tr style="border: none;">
                 <td class="text-xs" style="border: none;"> Reservation Time From: {{$reservations->reservation_time_from}} </td>
                <td class="text-xs" style="border: none;"> Reservation Time To:   {{ $reservations->reservation_time_to   }}</td>

            </tr>

        </table>


            <table border="5px" bordercolor="#8707B0">
                <thead>
                    <tr>
                    <th>Invoice Number</th>
                    <th class="text-center">Sales Details</th>
                    </tr>
                    </thead>
                <tbody>
                    <tr>
                    <td>{{ $salesInvoice->invoice_id->format('dmy') }}</td>
                    <td>
            <table border="1px" bordercolor="#F35557">

            <thead>
                <tr>
                    <th>Item Name</th>
                    <th>Price/pices</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
                </thead>
                @foreach ($salesInvoice->items as $key=> $saleitem )
                <tr>
                <td class="text-xs" dir="rtl" lang="ar" charset="utf-8"> {{$saleitem->name}} </td>
                <td class="text-xs">   {{ $saleitem->pivot['sale_price'] }}</td>
                <td class="text-xs">  {{ $saleitem->pivot['quantity'] }}</td>
                <td class="text-xs">  {{$saleitem->pivot['total']}}</td>

                @endforeach

            </tr>

            <p style="text-align:right;"><b>Total Balance: {{ $salesInvoice->total }} JD</b></p>
        </table>
    </td>

    </tr>

</tbody>
</table>
</div>

</body>
</html>
