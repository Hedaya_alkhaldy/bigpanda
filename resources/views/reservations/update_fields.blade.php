
</HEAD>
<BODY>


    <!-- Screen Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('screen_id', 'Screen :') !!}
        {!! Form::select('screen_id' ,$screens , null, ['class' => 'form-control ' ,'style'=>"height:35px "  ]) !!}

        <!-- Reservation Time Form Field -->
    <div class=" pt-3">
        {!! Form::label('reservation_time_from', 'Form:') !!}
        {!! Form::time('reservation_time_from', null, ['class' => 'form-control','id'=>'reservation_time_from']) !!}
    </div>
    </div>
    <!-- Reservation Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('reservation_date', 'Reservation Date:') !!}
        {!! Form::date('reservation_date', null, ['class' => 'form-control','id'=>'reservation_date',]) !!}
    </div>


    <!-- Reservation Time To Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('reservation_time_to', 'To:') !!}
        {!! Form::time('reservation_time_to', null, ['class' => 'form-control','id'=>'reservation_time_to']) !!}
    </div>

    <table class="table table-striped" id="dataTable">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Price/pices</th>
                <th>Total</th>
                <th>
                <button type="button"  class='btn btn-ghost-success'  onclick='addRow()' style='width:50px;'>
                    <i class="fa fa-plus"></i></button>
                </th>
            </tr>
            </thead>
    <tbody>

        <?php
         $qty = 0;
        $price = 0;
          $total = 0; ?>
       @foreach ($salesInvoice->items as $key=> $saleitem )
       <?php $qty = 'qty[row_'.$key.']';
       $price = 'price[row_'.$key.']';
       $total = 'total[row_'.$key.']';

       ?>

        <tr id='row_1'>
            <p>
                <td>
                     <div class="form-group col-sm-12">
                        <select name='item_id[]' class="form-control" style="width:200px;height: 35px;" readonly>
                            <option value="{{$saleitem->id }}" >{{$saleitem->name}}</option>
                        </select>
                                        </div>
                    </td>
                    <td>
                        <div class="form-group col-sm-12">
                            <input type="number" class="form-control" value={{ $saleitem->pivot['quantity'] }}  id='{{ $qty }}' name="qty[]" onChange="updateCalculate({{$key}})" >

                        </div>
                    </td>
                <td>
                    <div class="form-group col-sm-12">
                        <input list="price_id" id = 'price[row_1]' name="price[]" oninput="calculate()" onChange="updateCalculate(1)" class="form-control form-select" style="width:200px;"  value={{ $saleitem->pivot['sale_price'] }} />
                        <datalist id="price_id" >
                            @foreach ( $prices as $price )
                            <option  value="{{ $price['sale_price'] }}"  {{ $saleitem->pivot['sale_price'] }}== {{ $price['sale_price'] }}? selected:'' >{{ $price['sale_price'] }}</option>
                            @endforeach
                        </datalist>
                  </div>                    </div>
                </td>


                <td>
                    <div class="form-group col-sm-12">
                    <input type="text"  class="form-control lst" value={{ $saleitem->pivot['total'] }}  id ='{{ $total }}' name="total[]"  readonly>
                    <input type="text" hidden class="form-control" value={{ $saleitem->pivot['id'] }}   name="pivotId[]" >

                </div>
                </td>
                <td>
                    <div class="text-xs">


                        <a href="{{ route('order.item.destroy', ['reservationsId' => $reservations->id ,'salesInvoiceId' => $salesInvoice->id , 'item_id' => $saleitem->id  , 'pivotId'=>$saleitem->pivot['id']]) }}"
                            class ='btn btn-ghost-danger'>
                            <i class="fa fa-trash"></i>
                        </a>

                    </div>
                </td>
            </p>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="form-group form-inline col-sm-3 float-right">
    <input type="text" hidden readonly class="form-control ml-4 " id ='allQuantity' name="allQyantity" value={{ $salesInvoice->quantity }}>
</br>
    <h6 class="pt-3"><b>Total Balance :</b></h6>
    <input type="text" readonly class="form-control pt-3 ml-2" id ='allTotal' name="allTotal" value={{ $salesInvoice->total }}>
    </div>

<!-- Submit Field -->
<div class="form-group col-sm-12 pt-3">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('reservations.index') }}" class="btn btn-secondary">Cancel</a>
</div>


<script language="javascript">

    function calculate() {
        var rowCount = $('#dataTable tr').length-1;
        var myBox1 = document.getElementById('qty[row_'+rowCount+']').value;
        var myBox2 = document.getElementById('price[row_'+rowCount+']').value;
        var total = document.getElementById('total[row_'+rowCount+']');
        var myResult1 = myBox1 * myBox2 ;
        total.value = myResult1;
        totalAll(total);
        if(myBox2){
            quantityAll(myBox1);

        }




    }

    function totalAll(total) {

        myTotal = Number(myTotal) + Number(total.value);
        var allTotal = document.getElementById('allTotal');
        allTotal.value = Number(allTotal.value) + Number(myTotal);

    }

    function quantityAll(qty) {

        var allQty = document.getElementById('allQuantity');
        allQty.value = Number(allQty.value) + Number(qty);



    }

    function updateCalculate(id) {

        var rowCount = id;
        var myBox1 = document.getElementById('price[row_'+rowCount+']').value;
        var myBox2 = document.getElementById('qty[row_'+rowCount+']').value;
        var total = document.getElementById('total[row_'+rowCount+']');
        var myResult1 = myBox1 * myBox2 ;
        total.value = myResult1;
        updateQuantityAll();
        updateTotalAll();


    }

    function updateQuantityAll() {

        var allQty = document.getElementById('allQuantity');
        var rowCount = $('#dataTable tr').length-1;
        var sum =0;
        for (var i =0; i < rowCount; i++) {
            var qty = document.getElementById('qty[row_'+i+']').value;
            sum = Number(sum)+ Number(qty) ;
            allQty.value = Number(sum);
        }


    }

    function  updateTotalAll() {

        var allQty = document.getElementById('allTotal');
        var rowCount = $('#dataTable tr').length-1;
        var sum =0;
        for (var i =0; i < rowCount; i++) {
            var qty = document.getElementById('total[row_'+i+']').value;
            sum = Number(sum)+ Number(qty) ;
            allQty.value = Number(sum);

        }


    }

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script language="javascript">
    var myTotal =0;
    var myQuantity =0;
    var row=0;
    var total=0;
    function addRow()
{
    var rowCount = $('#dataTable tr').length-1;
    var table = document.getElementById("dataTable");
    var row = table.insertRow(rowCount+1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var qty = '[row_'.concat(rowCount,']');
    var price = '[row_'.concat(rowCount,']');
    var total = '[row_'.concat(rowCount,']');
    var abc = 'this is text'; <?php $abc = "<script>document.write(rowCount)</script>"?>
    cell1.innerHTML=' <div class="form-group col-sm-12"><input list="item_id" name="newItemId[]" class="form-control form-select" placeholder="search item" style="width:200px;  height: 35px;" />';
    cell1.innerHTML+='<datalist id="item_id" >@foreach ( $items as $item )<option  value="{{ $item['item_number'] }}">{{ $item['name'] }}</option>@endforeach </datalist></div>';
    cell2.innerHTML = '<div class="form-group col-sm-12"><input type="number" class="form-control"  name="newQty[]"  id="qty[row_'+rowCount+']" oninput="calculate()" onChange="updateCalculate('+rowCount+')" min="0" required></div>';
    cell3.innerHTML='<div class="form-group col-sm-12">@foreach ( $prices as $price )<input oninput="calculate()" onChange="updateCalculate('+rowCount+')" list="price" id="price[row_'+rowCount+']" onchange="getPrice('+rowCount+',{{ $item['price'] }})" id="price[row_'+rowCount+']" name="newPrice[]" class="form-control form-select" placeholder="search item" style="width:200px;" />';
        cell3.innerHTML+='<datalist id="price" ><option  value="{{ $price['sale_price'] }}">{{ $price['sale_price'] }}</option>@endforeach </datalist></div>';    cell4.innerHTML = '<div class="form-group col-sm-12"><input type="text"   class="form-control lst" name="newTotal[]" id="total[row_'+rowCount+']" readonly></div>';
    cell4.innerHTML = '<div class="form-group col-sm-12"><input type="text"   class="form-control lst" name="newTotal[]" id="total[row_'+rowCount+']" readonly></div>';
    $(this).focus().select();
    rowCount++;
}


function deleteRow() {
    var rowCount = $('#dataTable tr').length;
    var row=rowCount-1;

    if(row > 1)
    {
        document.getElementById("dataTable").deleteRow(rowCount-1);
    }
    if(row==1)
    {
        alert("the minimum number of rows should be 1. ");
    }
}
</script>
