<!-- Reservation Id Field -->
<div class="form-group">
    {!! Form::label('reservation_id', 'Reservation Id:') !!}
    <p>{{ $order->reservation_id }}</p>
</div>

<!-- Sale Invoice Id Field -->
<div class="form-group">
    {!! Form::label('sale_invoice_id', 'Sale Invoice Id:') !!}
    <p>{{ $order->sale_invoice_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $order->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $order->updated_at }}</p>
</div>

