<!-- Reservation Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reservation_id', 'Reservation Id:') !!}
    {!! Form::number('reservation_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Invoice Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sale_invoice_id', 'Sale Invoice Id:') !!}
    {!! Form::number('sale_invoice_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-secondary">Cancel</a>
</div>
