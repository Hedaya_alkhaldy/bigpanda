<li class="nav-item {{ Request::is('items*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('items.index') }}">
        <i class="nav-icon icon-social-dropbox
        "></i>
        <span>Items</span>
    </a>
</li>
<li class="nav-item {{ Request::is('providers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('providers.index') }}">
        <i class="nav-icon icon-people"></i>
        <span>Providers</span>
    </a>
</li>
<li class="nav-item {{ Request::is('purchaseInvoices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('purchaseInvoices.index') }}">
        <i class="nav-icon icon-chart"></i>
        <span>Purchase Invoices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('salesInvoices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('salesInvoices.index') }}">
        <i class="nav-icon icon-notebook"  ></i>
        <span>Sales Invoices</span>
    </a>
</li>

<li class="nav-item {{ Request::is('screens*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('screens.index') }}">
        <i class="nav-icon icon-screen-desktop"></i>
        <span>Screens</span>
    </a>
</li>

<li class="nav-item {{ Request::is('reservations*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('reservations.index') }}">
        <i class="nav-icon icon-check"></i>
        <span>Reservations</span>
    </a>
</li>

<li class="nav-item {{ Request::is('expenses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('expenses.index') }}">
        <i class="nav-icon icon-graph"></i>
        <span>Expenses</span>
    </a>
</li>

    <li class="nav-item">
        <a class="nav-link  text-truncate collapsed py-1 nav-link {{ Request::is('reports*') ? 'active' : '' }} py-0"
         href="#submenu1sub1" data-toggle="collapse" data-target="#submenu1sub1">
         <i class="nav-icon icon-docs"  ></i><span>Reports</span></a>
    <div class="collapse" id="submenu1sub1" aria-expanded="false">
        <ul class="flex-column nav pl-1">
            <li class="nav-item">
                <a class="nav-link " href="{{ route('reports.sales.index') }}">
                    <i class="nav-icon icon-doc"></i>
                    <span>Sales</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('reports.purchases.index') }}">
                    <i class="nav-icon icon-book-open"  ></i>
                    <span>Purchase</span>
                </a>
            </li>

        </ul>
    </div>
</li>
