<!-- Screen Id Field -->
<div class="form-group">
    {!! Form::label('screen_id', 'Screen Number:') !!}
    <p>{{ $screen->screen_name }}</p>
</div>

<!-- Screen Status Id Field -->
<div class="form-group">
    {!! Form::label('screen_status_id', 'Screen Status:') !!}
    <p>{{ $screen->screenStatus->name }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $screen->notes }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $screen->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $screen->updated_at }}</p>
</div>

