<div class="table-responsive-sm">
    <table class="table table-striped" id="screens-table">
        <thead>
            <tr>
                <th>Screen </th>
        <th>Screen Status </th>
        <th>Notes</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($screens as $screen)
            <tr>
                <td>{{ $screen->screen_name }}</td>
            <td>{{ $screen->screenStatus->name }}</td>
            <td>{{ $screen->notes }}</td>
                <td>
                    {!! Form::open(['route' => ['screens.destroy', $screen->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('screens.show', [$screen->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('screens.edit', [$screen->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
