<!-- Screen Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('screen_name', 'Screen :') !!}
    {!! Form::text('screen_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Screen Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('screen_status_id', 'Screen Status :') !!}
    {!! Form::select('screen_status_id' ,$screenStauts , null, ['class' => 'form-control ' ,'style'=>"height:35px "  ]) !!}

</div>

<!-- Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('notes', 'Notes:') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('screens.index') }}" class="btn btn-secondary">Cancel</a>
</div>
