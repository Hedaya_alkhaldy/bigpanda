<!DOCTYPE html>
<html>
<head>
    <title>Purchase Invoices</title>
</head>
<style>
    table, td, th {
      border: 1px solid #ddd;
      text-align: left;
      font-size: smaller;    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    h1,img{
        font-size: medium;
        text-align: center;

    }
    .img-contanier{
        text-align: center;
    }
    </style>
<body>

    <div class="table-responsive">
        <h1>Sales Invoices </h1>
        <h5>    {{ $date->format('d-m-20y') }}</h5>
            <table border="5px" bordercolor="#8707B0">
                <thead>
                    <tr>
                    <th><b> Invoice Number</b></th>
                    <th class="text-center">Sales Details</th>
                    </tr>
                    </thead>
                <tbody>
                    @foreach($salesInvoices as $salesInvoice)
                    <tr>
                    <td>{{ $salesInvoice->invoice_id->format('dmy') }}</td>
                    <td>
            <table border="1px" bordercolor="#F35557">

            <thead>
                <tr>
                <th><b>Item Name</b></th>
                <th><b>Quantity</b></th>
                <th><b>Price/pcs</b></th>
                <th><b>Total</b></th>
                </tr>
                </thead>
                @foreach($salesInvoice->items as $salesItem)
                <tr>
                    @foreach($items as $item)
                    @if($salesItem->id == $item->id )
                    <td class="text-xs">{{ $item->name }}  </td>




                <td class="text-xs">  {{$salesItem->pivot['quantity']}}</td>
                <td class="text-xs">  {{$salesItem->pivot['sale_price']}}</td>
                <td class="text-xs">  {{$salesItem->pivot['total']}}</td>
                @endif

                @endforeach

            </tr>

            @endforeach
        </table>
    </td>

    </tr>
       @endforeach
</tbody>
</table>
</div>
<p><b>Total Balance: {{ $salesInvoice->total }} JD</b></p>

</body>
</html>
