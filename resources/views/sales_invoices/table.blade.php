<div class="table-responsive-sm">
    <table class="table table-striped" id="salesInvoices-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Invoice Id</th>
                <th>Quantity</th>
                <th>Total</th>


                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($salesInvoices as $key => $salesInvoice)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $salesInvoice->invoice_id->format('dmy') }}</td>
                <td>{{ $salesInvoice->quantity }}</td>
                <td>{{$salesInvoice->total }}</td>


                <td>
                    {!! Form::open(['route' => ['salesInvoices.destroy', $salesInvoice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('salesInvoices.show', [$salesInvoice->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('salesInvoices.edit', [$salesInvoice->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
