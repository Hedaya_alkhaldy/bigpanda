@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('salesInvoices.index') }}">Sales Invoice</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>

     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('salesInvoices.index') }}" class="btn btn-light">Back</a>
                                  <div class='btn-group float-right'>
                                    <a href="{{ route('sales.pdf', [ 'id'=>$salesInvoice->id]) }}" class='btn btn-ghost-danger'><i class="fa fa-file-pdf-o"></i> Export as PDF</a>
                                    <a href="{{ route('sales.excel', [ 'id'=>$salesInvoice->id]) }}" class='btn btn-ghost-success'><i class="fa fa-file-excel-o"></i> Export as Excel</a>
                                </div>
                                </div>
                             <div class="card-body">
                                 @include('sales_invoices.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
