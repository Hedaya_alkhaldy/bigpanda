
<!-- Updated At Field -->
<div class="form-group">
    <p>{{ $salesInvoice->invoice_id->format('d-m-20y')  }}</p>
</div>

 <table class="table table-striped" id="dataTable">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Price/pices</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
            </thead>
    <tbody>

        <?php
         $qty = 0;
        $price = 0;
          $total = 0; ?>
       @foreach ($salesInvoice->items as $key=> $saleitem )
       <?php $qty = 'qty[row_'.$key.']';
       $price = 'price[row_'.$key.']';
       $total = 'total[row_'.$key.']';

       ?>

        <tr id='row_1'>
            <p>
                <td>
                     <div class="form-group col-sm-12">
                        {{$saleitem->name}}
                                        </div>
                    </td>
                <td>
                    <div class="form-group col-sm-12">
                        {{ $saleitem->pivot['sale_price'] }}
                                        </div>
                </td>
                <td>
                    <div class="form-group col-sm-12">
                        {{ $saleitem->pivot['quantity'] }}
                    </div>
                </td>

                <td>
                    <div class="form-group col-sm-12">
                        {{ $saleitem->pivot['total'] }}
                </div>
                </td>
                <td>
                </td>
            </p>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="form-group float-right">
   <h6 class="pt-3 "><b>Total Balance :     {{ $salesInvoice->total }}  </b></h6>


</div>
