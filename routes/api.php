<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-screen-status/{screen}', 'ScreenController@getStatus');
Route::get('get-reservation-time/{screen}', 'ScreenController@getReservationTime');
Route::get('check-reservation-time/{screen}', 'ScreenController@checkReservationTime');

