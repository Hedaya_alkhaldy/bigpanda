<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::resource('items', App\Http\Controllers\ItemController::class);


Route::resource('providers', App\Http\Controllers\ProviderController::class);


Route::resource('purchaseInvoices', App\Http\Controllers\PurchaseInvoiceController::class);


Route::resource('salesInvoices', App\Http\Controllers\SalesInvoiceController::class);

Route::get('sales/pdf', [App\Http\Controllers\PDFController::class, 'salesPDF'])->name('sales.pdf');
Route::get('sales/item/destroy', [App\Http\Controllers\SalesInvoiceController::class, 'destroyItem'])->name('sales.item.destroy');


Route::get('purchase/pdf', [App\Http\Controllers\PDFController::class, 'purchasePDF'])->name('purchase.pdf');
Route::get('purchaseinvoice/item/destroy', [App\Http\Controllers\PurchaseInvoiceController::class, 'destroyItem'])->name('purchase.item.destroy');
Route::get('export/excel', [App\Http\Controllers\SalesInvoiceController::class, 'exportExcel'])->name('sales.excel');
Route::get('export/purchase/excel', [App\Http\Controllers\PurchaseInvoiceController::class, 'exportExcel'])->name('purchase.excel');

Route::get('reports/purchases', [App\Http\Controllers\PurchaseReportController::class, 'index'])->name('reports.purchases.index');
Route::post('reports/purchases/show', [App\Http\Controllers\PurchaseReportController::class, 'show'])->name('purchases.report.show');
Route::get('export/report/purchase/excel', [App\Http\Controllers\PurchaseReportController::class, 'exportExcel'])
    ->name('purchase.report.excel');
Route::get('export/report/purchase/pdf', [App\Http\Controllers\PDFController::class, 'purchaseReportPDF'])
    ->name('purchase.report.pdf');
Route::get('reports/sale', [App\Http\Controllers\SalesReportController::class, 'index'])->name('reports.sales.index');
Route::post('reports/sale', [App\Http\Controllers\SalesReportController::class, 'show'])->name('sales.report.show');
Route::get('reports/sale/pdf', [App\Http\Controllers\PDFController::class, 'allSalesPDF'])->name('all.sales.pdf');
Route::get('reports/export/excel', [App\Http\Controllers\SalesInvoiceController::class, 'exportAllExcel'])->name('all.sales.excel');

Route::get('item/price/{id}', [App\Http\Controllers\ItemController::class, 'getItemPrice'])->name('item.price');



Route::resource('screens', App\Http\Controllers\ScreenController::class);


Route::resource('screenStatuses', App\Http\Controllers\ScreenStatusController::class);


Route::resource('reservations', App\Http\Controllers\ReservationsController::class);
Route::get('order/item/destroy', [App\Http\Controllers\ReservationsController::class, 'destroyItem'])->name('order.item.destroy');
Route::get('reservations/order/pdf', [App\Http\Controllers\PDFController::class, 'reservationsPDF'])->name('reservations.pdf');



Route::resource('orders', App\Http\Controllers\OrderController::class);


Route::resource('expenses', App\Http\Controllers\ExpenseController::class);
Route::post('expenses/search/date', [App\Http\Controllers\ExpenseController::class, 'expenseByDate'])->name('expenses.search');
Route::get('expenses/search/date', [App\Http\Controllers\ExpenseController::class, 'expenseByDate'])->name('expenses.search');
