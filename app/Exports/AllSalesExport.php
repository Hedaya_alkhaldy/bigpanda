<?php

namespace App\Exports;

use App\Models\SalesInvoices;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Item;
use App\Models\SalesInvoice;

class AllSalesExport implements FromView
{
    public function __construct(string $to,string $from)
    {
        $this->to = $to;
        $this->from = $from;

    }

    public function view():View
    {
        $salesInvoices = SalesInvoice::with('items')->whereBetween('invoice_id', array( $this->from,
        $this->to))->get();
        $items = Item::all();

        return view('reports.sales.excel')
        ->with(['salesInvoices' => $salesInvoices ,'items' => $items, 'to' => $this->to ,'from' => $this->from]);  
      }

}

