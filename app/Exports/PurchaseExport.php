<?php

namespace App\Exports;

use App\Models\Item;
use App\Models\PurchaseInvoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PurchaseExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return PurchaseInvoice::all();
    // }

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function view():View
    {
       $purchaseInvoices = PurchaseInvoice::where('id', $this->id)->with('items')->get();
        $items = Item::all();

      return view('purchase_invoices.excel',['purchaseInvoices' => $purchaseInvoices,'items' => $items]);
    }

}
