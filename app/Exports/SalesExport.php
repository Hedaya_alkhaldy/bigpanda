<?php

namespace App\Exports;

use App\Models\Item;
use App\Models\SalesInvoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\FromView;

class SalesExport implements FromView
{


    public function __construct(int $id)
    {
        $this->id = $id;
    }

    // public function headings():array{
    //     return[
    //         'invoice_id',
    //         'quantity',
    //         'total'
    //     ];

    // }

    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {

    //     return collect(SalesInvoice::getEmployee( $this->id));
    // }

    public function view():View
    {
        $salesInvoices = SalesInvoice::where('id', $this->id)->with('items')->get();
        $items = Item::all();
        $date = '';

        foreach ($salesInvoices as $salesInvoice)
        {
            $date = $salesInvoice->invoice_id;

        }
      return view('sales_invoices.excel',['salesInvoices' => $salesInvoices,'items' => $items ,'date' =>$date]);
    }

  

}
