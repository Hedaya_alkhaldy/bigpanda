<?php

namespace App\Exports;

use App\Models\Item;
use App\Models\PurchaseInvoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PurchaseReportExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    // public function collection()
    // {
    //     return PurchaseInvoice::all();
    // }

    public function __construct(string $from, string $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function view(): View
    {
        $purchaseInvoices = PurchaseInvoice::whereBetween('invoice_id', array($this->from, $this->to))->with('items')->get();
        $items = Item::all();

        return view('reports.purchase.excel', ['from' => $this->from, 'to' => $this->to, 'purchaseInvoices' => $purchaseInvoices,
            'items' => $items]);
    }
}
