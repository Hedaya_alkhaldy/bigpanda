<?php

namespace App\Repositories;

use App\Models\Expense;
use App\Repositories\BaseRepository;

/**
 * Class ExpenseRepository
 * @package App\Repositories
 * @version April 5, 2022, 9:24 am UTC
*/

class ExpenseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reason',
        'amount',
        'expense_date',
        'notes'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Expense::class;
    }
}
