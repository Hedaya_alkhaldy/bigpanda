<?php

namespace App\Repositories;

use App\Models\Reservations;
use App\Repositories\BaseRepository;

/**
 * Class ReservationsRepository
 * @package App\Repositories
 * @version March 30, 2022, 11:28 am UTC
*/

class ReservationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'screen_id',
        'reservation_time_from',
        'reservation_time_to',
        'reservation_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reservations::class;
    }
}
