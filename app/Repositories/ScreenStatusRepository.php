<?php

namespace App\Repositories;

use App\Models\ScreenStatus;
use App\Repositories\BaseRepository;

/**
 * Class ScreenStatusRepository
 * @package App\Repositories
 * @version March 30, 2022, 11:11 am UTC
*/

class ScreenStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ScreenStatus::class;
    }
}
