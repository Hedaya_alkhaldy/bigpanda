<?php

namespace App\Repositories;

use App\Models\SalesInvoice;
use App\Repositories\BaseRepository;

/**
 * Class SalesInvoiceRepository
 * @package App\Repositories
 * @version January 5, 2022, 10:52 am UTC
*/

class SalesInvoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SalesInvoice::class;
    }
}
