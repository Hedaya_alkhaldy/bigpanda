<?php

namespace App\Repositories;

use App\Models\PurchaseInvoice;
use App\Repositories\BaseRepository;

/**
 * Class PurchaseInvoiceRepository
 * @package App\Repositories
 * @version January 5, 2022, 10:47 am UTC
*/

class PurchaseInvoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'provider_id',
        'type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PurchaseInvoice::class;
    }
}
