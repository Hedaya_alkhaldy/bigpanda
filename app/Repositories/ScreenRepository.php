<?php

namespace App\Repositories;

use App\Models\Screen;
use App\Repositories\BaseRepository;

/**
 * Class ScreenRepository
 * @package App\Repositories
 * @version March 30, 2022, 11:11 am UTC
*/

class ScreenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'screen_id',
        'screen_status_id',
        'notes'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Screen::class;
    }
}
