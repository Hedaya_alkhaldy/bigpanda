<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReservationsRequest;
use App\Http\Requests\UpdateReservationsRequest;
use App\Repositories\ReservationsRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PriceItemResource;
use App\Models\Item;
use App\Models\Order;
use App\Models\Reservations;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\SalesInvoice;
use App\Models\Screen;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;


use Flash;
use Response;

use function PHPUnit\Framework\isEmpty;

class ReservationsController extends AppBaseController
{
    /** @var  ReservationsRepository */
    private $reservationsRepository;

    public function __construct(ReservationsRepository $reservationsRepo)
    {
        $this->reservationsRepository = $reservationsRepo;
    }

    /**
     * Display a listing of the Reservations.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $reservations = Reservations::with('screens')->get();
        if (empty($reservations)) {
            Flash::error('Reservations not found');


            return view('reservations.index');

        }else{

        return view('reservations.index')
            ->with('reservations', $reservations);
    }
    }

    /**
     * Show the form for creating a new Reservations.
     *
     * @return Response
     */
    public function create()
    {
        $screens = Screen::where('screen_status_id',1)->pluck('screen_name', 'id');
        $allItems =  Item::with('purchaseInvoices')->get();
        $mytime = Carbon::now();
        $date = $mytime->toDateTimeString() ;
        $date = date('Y-m-d');
        $prices = Item::select('sale_price')->get();
        if($prices->isEmpty()){
            Flash::success('You Not Have Any price , please check your item first.');

            return redirect(route('reservations.index'));

        }elseif($screens->isEmpty()){
            Flash::success('You Not Have Any screens , please check your screens first.');

            return redirect(route('reservations.index'));


        }else{
            $items = array();
        foreach($allItems as $item){
            if($item->purchaseInvoices->isEmpty()){

                $newItem = ['id'=>$item->id ,'item_number'=>$item->item_number, 'name' => $item->name , 'price' =>0];
                array_push($items, $newItem);
            }else{
                foreach($item->purchaseInvoices as $itemPrice){
                $newItem = ['id'=>$item->id ,'item_number'=>$item->item_number , 'name' => $item->name , 'price' =>$itemPrice->pivot->purchasing_price];
                array_push($items, $newItem);
            }
        }

    }

    // dd($items);

        // $items = PriceItemResource::collection($allItems);

        return view('reservations.create')->with(['prices'=>$prices, 'screens'=> $screens,'items' => $items,'date'=>$date]);
        }


    }


    /**
     * Store a newly created Reservations in storage.
     *
     * @param CreateReservationsRequest $request
     *
     * @return Response
     */
    public function store(CreateReservationsRequest $request)
    {
        if (Auth::check()) {
            $input = $request->all();
            //  dd($input);
            $mytime = Carbon::now();
            $date = $mytime->toDateTimeString() ;
            $date = date('Y-m-d');
            $input['reservation_date'] = $input['reservation_date']?$input['reservation_date']:$date;

            // dd($input['item_id'][0]);
            if($input['item_id'][0] != null){
                $items=$input['item_id'];
                $itemId=[];
                foreach($items  as $item)
                {
                    if($item!=null)
                    {
                        $id=Item::where('item_number',$item)->first();
                        array_push($itemId,$id);
                    }
                }
                $arraySize = count($itemId);
            }else{
                $arraySize=0;
            }



            DB::beginTransaction();
            try {

                $screen = Screen::where('id',$input['screen_id'])->first();

                $reservations = $this->reservationsRepository->create($input);
                $screen ->update([
                    'screen_status_id' =>2
                ]);

                $salesInvoice = SalesInvoice::create([
                    'invoice_id' => $input['reservation_date'],
                     'quantity' => $input['allQuantity']?$input['allQuantity']:0,
                    'total' => $input['allTotal'] ?$input['allTotal'] :0]);

                    $total = $input['total']?$input['total']:0;
                    $quantity = $input['qty']?$input['qty']:0;
                    $price = $input['price']?$input['price']:0;

                for ($i = 0; $i < $arraySize; $i++) {
                    $salesInvoice->items()->attach(
                        $itemId[$i], [
                            'quantity' => $quantity[$i],
                            'sale_price' => $price[$i]?$price[$i]:$price[$i+1],
                            'total' => $total[$i]

                        ]
                    );

                }

                $order = Order::create([
                    'reservation_id' => $reservations->id,
                    'sale_invoice_id' => $salesInvoice->id
                ]);
                    DB::commit();
                    Flash::success('Reservations saved successfully.');


                    return redirect(route('reservations.index'));
                } catch (\Exception $e) {
                DB::rollback();
                Flash::error('Created failed'.$e);
                return redirect(route('reservations.index'));

            }
        }
        else{
            return Redirect::to('/login');
        }



    }

    /**
     * Display the specified Reservations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reservations = Reservations::where('id',$id)->with('screens')->first();
        $screens = Screen::pluck('screen_name', 'id');
        $order = Order::where('reservation_id',$reservations->id)->first();
        $salesInvoice = SalesInvoice::where('id',$order->sale_invoice_id)->first();
        //dd($order,$salesInvoice);
        $items = Item::all();

        if (empty($reservations)) {
            Flash::error('Reservations not found');

            return redirect(route('reservations.index'));
        }

        return view('reservations.show')->with(['reservations'=> $reservations , 'salesInvoice' => $salesInvoice ,'screens' => $screens,'items' => $items]);
    }

    /**
     * Show the form for editing the specified Reservations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reservations = $this->reservationsRepository->find($id);
        $screens = Screen::pluck('screen_name', 'id');
        $order = Order::where('reservation_id',$reservations->id)->first();
        $salesInvoice = SalesInvoice::where('id',$order->sale_invoice_id)->first();
        //dd($order,$salesInvoice);
        $items = Item::all();
        $prices = Item::select('sale_price')->get();



        if (empty($reservations)) {
            Flash::error('Reservations not found');

            return redirect(route('reservations.index'));
        }

        return view('reservations.edit')->with(['reservations'=> $reservations , 'salesInvoice' => $salesInvoice ,
        'screens' => $screens,'items' => $items, 'prices' => $prices]);
    }

    /**
     * Update the specified Reservations in storage.
     *
     * @param int $id
     * @param UpdateReservationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReservationsRequest $request)
    {
        if (Auth::check()) {
            $input = $request->all();
            // dd($input);
            $reservations = $this->reservationsRepository->find($id);

            if (empty($reservations)) {
                Flash::error('Reservations not found');

                return redirect(route('reservations.index'));
            }

            DB::beginTransaction();
            try {

                $reservations = $this->reservationsRepository->update($request->all(), $id);
                $order = Order::where('reservation_id',$reservations->id)->first();
                $salesInvoice = SalesInvoice::where('id',$order->sale_invoice_id)->first();


                $salesInvoice ->update([
                    'invoice_id' => $input['reservation_date'],
                    'quantity' => $input['allQyantity'],
                   'total' => $input['allTotal']
                ]);

                if($request['newItemId']){
                    $newArraySize = count($request['newItemId']);
                    $newTotal = $request['newTotal'];
                    $newQty = $request['newQty'];
                    $newPrice = [];
                    foreach($request['newPrice'] as $price){
                        if($price !== null){
                            array_push($newPrice,$price);

                        }
                    }
                    $items=$request['newItemId'];
                    $newItemId=[];
                    foreach($items  as $item)
                    {
                        $id=Item::where('item_number',$item)->first()->id;
                        array_push($newItemId,$id);

                    }

                for ($i = 0; $i < $newArraySize; $i++) {
                    $salesInvoice->items()->attach(
                        $newItemId[$i], [
                            'quantity' => $newQty[$i],
                            'sale_price' => $newPrice[$i],
                            'total' => $newTotal[$i]

                        ]);
                }
                }
                if($request['item_id']){
                $total = $request['total'];
                $quantity = $request['qty'];
                $price = $request['price'];
                $itemId = $request['item_id'];
                $pivotId = $request['pivotId'];
                $arraySize = count($request['item_id']);
            for ($i = 0; $i < $arraySize; $i++) {
                $salesInvoice->items()->wherePivot('id',$pivotId[$i])->updateExistingPivot(
                 $itemId[$i], [
                        'quantity' => $quantity[$i],
                        'sale_price' => $price[$i],
                        'total' => $total[$i]
                    ]
                );
            }
        }
                    DB::commit();

                    Flash::success('Reservations updated successfully.');

                    return redirect(route('reservations.index'));
            } catch (\Exception $e) {
                dd($e);
                DB::rollback();
                Flash::error('Created failed');
                return redirect(route('reservations.index'));

            }



        }
        else{
            return Redirect::to('/login');
        }
    }

    /**
     * Remove the specified Reservations from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reservations = $this->reservationsRepository->find($id);
        $order = Order::where('reservation_id',$reservations->id)->first();
        $salesInvoice = SalesInvoice::where('id',$order->sale_invoice_id)->first();

        if (empty($reservations)) {
            Flash::error('Reservations not found');

            return redirect(route('reservations.index'));
        }

        $this->reservationsRepository->delete($id);
        $order->delete();
        $salesInvoice->delete();


        Flash::success('Reservations deleted successfully.');

        return redirect(route('reservations.index'));
    }


     /**
     * Remove the specified  item that belong PurchaseInvoice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */

    public function destroyItem(Request $request)
    {
        if (Auth::check()) {
        $input=$request->all();
        $saleInvoiceId = (int)$request['salesInvoiceId'];
        $pivotId=$input['pivotId'];
        DB::beginTransaction();
        try {

            $item_id=$input['item_id'];
            $salesInvoice = SalesInvoice::where('id',$saleInvoiceId)->first();
            $item = $salesInvoice->items()->wherePivot('id',$pivotId)->detach($item_id);
            $total=0;
            $quantity=0;
            foreach($salesInvoice->items as $invoiceItem)
            {
                $total+=$invoiceItem->pivot['total'];
                $quantity+=$invoiceItem->pivot['quantity'];
            }
            $salesInvoice->update(['total'=>$total , 'quantity'=>$quantity]);

        DB::commit();

        //Back to Update Fields
        $reservations = $this->reservationsRepository->find($request['reservationsId']);
        $screens = Screen::pluck('screen_name', 'id');
        $order = Order::where('reservation_id',$reservations->id)->first();
        $salesInvoice = SalesInvoice::where('id',$order->sale_invoice_id)->first();
        $prices = Item::select('sale_price')->get();
        //dd($order,$salesInvoice);
        $items = Item::all();


        if (empty($reservations)) {
            Flash::error('Reservations not found');

            return redirect(route('reservations.index'));
        }

        return view('reservations.edit')->with(['reservations'=> $reservations , 'prices' => $prices,'salesInvoice' => $salesInvoice ,'screens' => $screens,'items' => $items]);

    }catch (\Exception $e) {
            dd($e);
            DB::rollback();
            Flash::error('Deleted failed');
            return redirect(route('salesInvoices.index'));

        }


    }
    else{
        return Redirect::to('/login');
    }
}

}



