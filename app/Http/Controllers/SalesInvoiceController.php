<?php

namespace App\Http\Controllers;

use App\Exports\SalesExport;
use App\Http\Requests\CreateSalesInvoiceRequest;
use App\Http\Requests\UpdateSalesInvoiceRequest;
use App\Repositories\SalesInvoiceRepository;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AllSalesExport;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\Item;
use App\Models\SalesInvoice;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Flash;
use Response;
use DB;

class SalesInvoiceController extends AppBaseController
{
    /** @var  SalesInvoiceRepository */
    private $salesInvoiceRepository;

    public function __construct(SalesInvoiceRepository $salesInvoiceRepo)
    {
        $this->salesInvoiceRepository = $salesInvoiceRepo;
    }

    /**
     * Display a listing of the SalesInvoice.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (Auth::check()) {

        $salesInvoices = SalesInvoice::with('items')->get();

        return view('sales_invoices.index')
            ->with('salesInvoices', $salesInvoices);
        }
        else{
            return Redirect::to('/login');
        }
    }

    /**
     * Show the form for creating a new SalesInvoice.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::check()) {

        $items = Item::all();
        $invoice_id =  Carbon::now()->toDateString();
        $formType = 'create';

        return view('sales_invoices.create')->with(['items' => $items, 'invoice_id' => $invoice_id , 'formType' =>$formType]);
        }
        else{
            return Redirect::to('/login');
        }
    }

    /**
     * Store a newly created SalesInvoice in storage.
     *
     * @param CreateSalesInvoiceRequest $request
     *
     * @return Response
     */
    public function store(CreateSalesInvoiceRequest $request)
    {
        if (Auth::check()) {
        $input = $request->all();
        $items=$input['item_id'];
        $itemId=[];
        foreach($items  as $item)
        {
            $id=Item::where('item_number',$item)->first()->id;
            array_push($itemId,$id);

        }
        $arraySize = count($input['item_id']);

        DB::beginTransaction();
        try {

            $salesInvoice = SalesInvoice::create([
                'invoice_id' => $input['date'],
                 'quantity' => $input['allQyantity'],
                'total' => $input['allTotal'] ]);

                $total = $input['total'];
                $quantity = $input['qty'];
                $price = $input['price'];

            for ($i = 0; $i < $arraySize; $i++) {
                $salesInvoice->items()->attach(
                    $itemId[$i], [
                        'quantity' => $quantity[$i],
                        'sale_price' => $price[$i],
                        'total' => $total[$i]

                    ]
                );

            }
                DB::commit();
                Flash::success('Sales Invoice saved successfully.');

                return redirect(route('salesInvoices.index'));
        } catch (\Exception $e) {
            DB::rollback();
            Flash::error('Created failed'.$e);
            return redirect(route('salesInvoices.index'));

        }
    }
    else{
        return Redirect::to('/login');
    }
}

    /**
     * Display the specified SalesInvoice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (Auth::check()) {
        $salesInvoice = $this->salesInvoiceRepository->find($id);

        if (empty($salesInvoice)) {
            Flash::error('Sales Invoice not found');

            return redirect(route('salesInvoices.index'));
        }
        $items = Item::all();
        $invoice_id = SalesInvoice::select('invoice_id')->where('id',$id)->first()['invoice_id'];

        return view('sales_invoices.show')->with(['salesInvoice' => $salesInvoice ,'items' => $items ,'invoice_id' => $invoice_id]);
    }
    else{
        return Redirect::to('/login');
    }
}

    /**
     * Show the form for editing the specified SalesInvoice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
        $salesInvoice = $this->salesInvoiceRepository->find($id);

        if (empty($salesInvoice)) {
            Flash::error('Sales Invoice not found');

            return redirect(route('salesInvoices.index'));
        }
        $items = Item::all();
        $invoice_id = SalesInvoice::select('invoice_id')->where('id',$id)->first()['invoice_id'];


        return view('sales_invoices.edit')->with(['salesInvoice' => $salesInvoice ,'items' => $items ,'invoice_id' => $invoice_id]);
    }
    else{
        return Redirect::to('/login');
    }
}

    /**
     * Update the specified SalesInvoice in storage.
     *
     * @param int $id
     * @param UpdateSalesInvoiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSalesInvoiceRequest $request)
    {
        if (Auth::check()) {
        $salesInvoice = SalesInvoice::find($id)->first();

        if (empty($salesInvoice)) {
            Flash::error('Sales Invoice not found');

            return redirect(route('salesInvoices.index'));
        }
        DB::beginTransaction();
        try {

            $salesInvoice = $this->salesInvoiceRepository->update([
                'invoice_id' => $request['date'],
                'quantity' => $request['allQyantity'],
                'total' => $request['allTotal']
            ], $id);

            if($request['newItemId']){
                $newArraySize = count($request['newItemId']);
                $newTotal = $request['newTotal'];
                $newQty = $request['newQty'];
                $newPrice = $request['newPrice'];
                $items=$request['newItemId'];
                $newItemId=[];
                foreach($items  as $item)
                {
                    $id=Item::where('item_number',$item)->first()->id;
                    array_push($newItemId,$id);

                }

            for ($i = 0; $i < $newArraySize; $i++) {
                $salesInvoice->items()->attach(
                    $newItemId[$i], [
                        'quantity' => $newQty[$i],
                        'sale_price' => $newPrice[$i],
                        'total' => $newTotal[$i]

                    ]);
            }
            }
            if($request['item_id']){
            $total = $request['total'];
            $quantity = $request['qty'];
            $price = $request['price'];
            $itemId = $request['item_id'];
            $pivotId = $request['pivotId'];
            $arraySize = count($request['item_id']);
        for ($i = 0; $i < $arraySize; $i++) {
            $salesInvoice->items()->wherePivot('id',$pivotId[$i])->updateExistingPivot(
             $itemId[$i], [
                    'quantity' => $quantity[$i],
                    'sale_price' => $price[$i],
                    'total' => $total[$i]
                ]
            );
        }
    }
                DB::commit();
                Flash::success('Sales Invoice updated successfully.');

                return redirect(route('salesInvoices.index'));
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            Flash::error('Created failed');
            return redirect(route('salesInvoices.index'));

        }



    }
    else{
        return Redirect::to('/login');
    }
}
     /**
     * Remove the specified  item that belong PurchaseInvoice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */

    public function destroyItem(Request $request)
    {
        if (Auth::check()) {
        $input=$request->all();
        $saleInvoiceId = (int)$request['salesInvoiceId'];
        $pivotId=$input['pivotId'];
        DB::beginTransaction();
        try {

            $item_id=$input['item_id'];
            $salesInvoice = $this->salesInvoiceRepository->find($saleInvoiceId);
            $item = $salesInvoice->items()->wherePivot('id',$pivotId)->detach($item_id);
            $total=0;
            $quantity=0;
            foreach($salesInvoice->items as $invoiceItem)
            {
                $total+=$invoiceItem->pivot['total'];
                $quantity+=$invoiceItem->pivot['quantity'];
            }
            $salesInvoice= $this->salesInvoiceRepository->update(['total'=>$total , 'quantity'=>$quantity], $saleInvoiceId);

        DB::commit();

        //Back to Update Fields
        $items = Item::all();
        $invoice_id =  Carbon::now()->toDateString();
        Flash::success('Item Removed from Sales Invoice successfully.');

        return view('sales_invoices.edit')->with(['salesInvoice' => $salesInvoice ,'items' => $items ,'invoice_id' => $invoice_id]);
        }
        catch (\Exception $e) {
            dd($e);
            DB::rollback();
            Flash::error('Deleted failed');
            return redirect(route('salesInvoices.index'));

        }


    }
    else{
        return Redirect::to('/login');
    }
}

    /**
     * Remove the specified SalesInvoice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
        $salesInvoice = $this->salesInvoiceRepository->find($id);

        if (empty($salesInvoice)) {
            Flash::error('Sales Invoice not found');

            return redirect(route('salesInvoices.index'));
        }

        $this->salesInvoiceRepository->delete($id);

        Flash::success('Sales Invoice deleted successfully.');

        return redirect(route('salesInvoices.index'));
    }
    else{
        return Redirect::to('/login');
    }
}

    public function exportExcel(Request $request){

        if (Auth::check()) {
        return Excel::download(new SalesExport($request['id']),'sales.xlsx');
    }
    else{
        return Redirect::to('/login');
    }
}

    public function exportAllExcel(Request $request){
        if (Auth::check()) {
        return Excel::download(new AllSalesExport($request['to'],$request['from']) ,'Sales Report ' . $request['from'] . '-' . $request['to'] . '.xlsx');
    }
    else{
        return Redirect::to('/login');
    }
}

}
