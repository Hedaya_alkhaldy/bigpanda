<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateScreenStatusRequest;
use App\Http\Requests\UpdateScreenStatusRequest;
use App\Repositories\ScreenStatusRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ScreenStatusController extends AppBaseController
{
    /** @var  ScreenStatusRepository */
    private $screenStatusRepository;

    public function __construct(ScreenStatusRepository $screenStatusRepo)
    {
        $this->screenStatusRepository = $screenStatusRepo;
    }

    /**
     * Display a listing of the ScreenStatus.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $screenStatuses = $this->screenStatusRepository->all();

        return view('screen_statuses.index')
            ->with('screenStatuses', $screenStatuses);
    }

    /**
     * Show the form for creating a new ScreenStatus.
     *
     * @return Response
     */
    public function create()
    {
        return view('screen_statuses.create');
    }

    /**
     * Store a newly created ScreenStatus in storage.
     *
     * @param CreateScreenStatusRequest $request
     *
     * @return Response
     */
    public function store(CreateScreenStatusRequest $request)
    {
        $input = $request->all();

        $screenStatus = $this->screenStatusRepository->create($input);

        Flash::success('Screen Status saved successfully.');

        return redirect(route('screenStatuses.index'));
    }

    /**
     * Display the specified ScreenStatus.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $screenStatus = $this->screenStatusRepository->find($id);

        if (empty($screenStatus)) {
            Flash::error('Screen Status not found');

            return redirect(route('screenStatuses.index'));
        }

        return view('screen_statuses.show')->with('screenStatus', $screenStatus);
    }

    /**
     * Show the form for editing the specified ScreenStatus.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $screenStatus = $this->screenStatusRepository->find($id);

        if (empty($screenStatus)) {
            Flash::error('Screen Status not found');

            return redirect(route('screenStatuses.index'));
        }

        return view('screen_statuses.edit')->with('screenStatus', $screenStatus);
    }

    /**
     * Update the specified ScreenStatus in storage.
     *
     * @param int $id
     * @param UpdateScreenStatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateScreenStatusRequest $request)
    {
        $screenStatus = $this->screenStatusRepository->find($id);

        if (empty($screenStatus)) {
            Flash::error('Screen Status not found');

            return redirect(route('screenStatuses.index'));
        }

        $screenStatus = $this->screenStatusRepository->update($request->all(), $id);

        Flash::success('Screen Status updated successfully.');

        return redirect(route('screenStatuses.index'));
    }

    /**
     * Remove the specified ScreenStatus from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $screenStatus = $this->screenStatusRepository->find($id);

        if (empty($screenStatus)) {
            Flash::error('Screen Status not found');

            return redirect(route('screenStatuses.index'));
        }

        $this->screenStatusRepository->delete($id);

        Flash::success('Screen Status deleted successfully.');

        return redirect(route('screenStatuses.index'));
    }
}
