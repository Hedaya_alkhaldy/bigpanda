<?php

namespace App\Http\Controllers;

use App\Exports\PurchaseExport;
use App\Http\Requests\CreatePurchaseInvoiceRequest;
use App\Http\Requests\UpdatePurchaseInvoiceRequest;
use App\Repositories\PurchaseInvoiceRepository;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\Item;
use App\Models\Provider;
use App\Models\PurchaseInvoice;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Flash;
use Response;
use DB;

class PurchaseInvoiceController extends AppBaseController
{
    /** @var  PurchaseInvoiceRepository */
    private $purchaseInvoiceRepository;

    public function __construct(PurchaseInvoiceRepository $purchaseInvoiceRepo)
    {
        $this->purchaseInvoiceRepository = $purchaseInvoiceRepo;
    }

    /**
     * Display a listing of the PurchaseInvoice.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (Auth::check())
        {
        $purchaseInvoices = $this->purchaseInvoiceRepository->all();
        $providers = Provider::all()->pluck('name', 'id');

        return view('purchase_invoices.index')
            ->with(['purchaseInvoices'=>$purchaseInvoices , 'providers'=>$providers]);
        }
        else{
            return Redirect::to('/login');
        }
    }

    /**
     * Show the form for creating a new PurchaseInvoice.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::check())
        {
        $providers=Provider::all()->pluck('name','id');
        $items=Item::all();
        return view('purchase_invoices.create')->with(['providers'=>$providers , 'items'=>$items]);
        }
        else{
            return Redirect::to('/login');
        }
    }

    /**
     * Store a newly created PurchaseInvoice in storage.
     *
     * @param CreatePurchaseInvoiceRequest $request
     *
     * @return Response
     */
    public function store(CreatePurchaseInvoiceRequest $request)
    {
        if (Auth::check())
        {
        $input = $request->all();
        $items=$input['item_id'];
        $itemId=[];
        foreach($items  as $item)
        {
            $id=Item::where('item_number',$item)->first()->id;
            array_push($itemId,$id);

        }

        $arraySize = count($input['item_id']);
        DB::beginTransaction();
        try {

            $purchaseInvoice = PurchaseInvoice::create([
                'invoice_id' => Carbon::now()->toDateString(),
                'provider_id'=>$input['provider_id'],
                'type'=>$input['type'],
                'quantity' => $input['allQuantity'],
                'total' => $input['allTotal'] ]);

                $total = $input['total'];
                $quantity = $input['qty'];
                $price = $input['price'];

            for ($i = 0; $i < $arraySize; $i++) {
                $purchaseInvoice->items()->attach(
                    $itemId[$i], [
                        'quantity' => $quantity[$i],
                        'purchasing_price' => $price[$i],
                        'total' => $total[$i]

                    ]
                );

            }
                DB::commit();
                Flash::success('purchase Invoice saved successfully.');

                return redirect(route('purchaseInvoices.index'));
        }
        catch (\Exception $e) {
            dd($e);
            DB::rollback();
            Flash::error('Created failed');
            return redirect(route('purchaseInvoices.index'));

        }
    }
    else{
        return Redirect::to('/login');
    }

    }

    /**
     * Display the specified PurchaseInvoice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (Auth::check())
        {
        $purchaseInvoice = PurchaseInvoice::with('items')->where('id', $id)->get()->first();
        if (empty($purchaseInvoice)) {
            Flash::error('Purchase Invoice not found');

            return redirect(route('purchaseInvoices.index'));
        }

        return view('purchase_invoices.show')->with('purchaseInvoice', $purchaseInvoice);
    }
    else{
        return Redirect::to('/login');
    }
    }

    /**
     * Show the form for editing the specified PurchaseInvoice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::check())
        {
        $purchaseInvoice = $this->purchaseInvoiceRepository->find($id);
        $providers = Provider::all()->pluck('name', 'id');
        $items=Item::all();

        if (empty($purchaseInvoice)) {
            Flash::error('Purchase Invoice not found');

            return redirect(route('purchaseInvoices.index'));
        }

        return view('purchase_invoices.edit')->with(
            ['purchaseInvoice'=> $purchaseInvoice , 'providers'=>$providers,
            'items'=>$items]);
        }
        else{
            return Redirect::to('/login');
        }
    }

    /**
     * Update the specified PurchaseInvoice in storage.
     *
     * @param int $id
     * @param UpdatePurchaseInvoiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePurchaseInvoiceRequest $request)
    {
        if (Auth::check())
        {
        $purchaseInvoice = $this->purchaseInvoiceRepository->find($id);
        $input=$request->all();

        DB::beginTransaction();
        try {
            $purchaseInvoice = $this->purchaseInvoiceRepository->update([
                'provider_id'=>$input['provider_id'],
                'type'=>$input['type'],
                'quantity' => $input['allQuantity'],
                'total' => $input['allTotal']
                ], $id);

            if($request->newItemId)
            {
                $items=$input['newItemId'];
                $newItemId=[];
                foreach($items  as $item)
                {
                    $id=Item::where('item_number',$item)->first()->id;
                    array_push($newItemId,$id);

                }
                $newCount=count($newItemId);
                $newPrice=$input['newPrice'];
                $newQty=$input['newQty'];
                $newTotal=$input['newTotal'];

                for ($i = 0; $i < $newCount; $i++) {
                    $purchaseInvoice->items()->attach(
                        $newItemId[$i], [
                            'quantity' => $newQty[$i],
                            'purchasing_price' => $newPrice[$i],
                            'total' => $newTotal[$i]
                        ]);
            }
            }

            if($request->item_id)
            {
                $total = $input['total'];
                $quantity = $input['qty'];
                $price = $input['price'];
                $itemId = $input['item_id'];
                $arraySize = count($input['item_id']);
                $pivotId=$input['pivotId'];

                for($i=0 ; $i<$arraySize ; $i++)
                {
                    $purchaseInvoice->items()->wherePivot('id',$pivotId[$i])->updateExistingPivot($itemId[$i], [
                        'quantity' => $quantity[$i],
                        'purchasing_price' => $price[$i],
                        'total' => $total[$i]
                    ], false);

                }

            }


                DB::commit();
                Flash::success('purchase Invoice update successfully.');

                return redirect(route('purchaseInvoices.index'));
        }
        catch (\Exception $e) {
            dd($e);
            DB::rollback();
            Flash::error('update failed');
            return redirect(route('purchaseInvoices.index'));

        }



        Flash::success('Purchase Invoice updated successfully.');

        return redirect(route('purchaseInvoices.index'));
    }
    else{
        return Redirect::to('/login');
    }
    }

      /**
     * Remove the specified  item that belong PurchaseInvoice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */

    public function destroyItem(Request $request)
    {
        if (Auth::check())
        {
        $input=$request->all();
        $purchaseInvoiceId = (int)$request->purchaseInvoiceId;
        $pivotId=$input['pivotId'];
        DB::beginTransaction();
        try {

            $item_id=$input['item_id'];
            $purchaseInvoice = $this->purchaseInvoiceRepository->find($purchaseInvoiceId);
            $item = $purchaseInvoice->items()->wherePivot('id',$pivotId)->detach($item_id);
            $total=0;
            $quantity=0;
            foreach($purchaseInvoice->items as $invoiceItem)
            {
                $total+=$invoiceItem->pivot['total'];
                $quantity+=$invoiceItem->pivot['quantity'];
            }
            $purchaseInvoice= $this->purchaseInvoiceRepository->update(['total'=>$total , 'quantity'=>$quantity], $purchaseInvoiceId);

        DB::commit();

        //Back to Update Fields
        $providers = Provider::all()->pluck('name', 'id');
        $items=Item::all();
        Flash::success('Item Removed from Purchase Invoice successfully.');

        return view('purchase_invoices.edit')->with([
            'purchaseInvoice' => $purchaseInvoice,
            'providers' => $providers, 'items' => $items
        ]);
        }
        catch (\Exception $e) {
            dd($e);
            DB::rollback();
            Flash::error('Deleted failed');
            return redirect(route('purchaseInvoices.index'));

        }
    }
    else{
        return Redirect::to('/login');
    }


    }

    /**
     * Remove the specified PurchaseInvoice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::check())
        {
        $purchaseInvoice = $this->purchaseInvoiceRepository->find($id);

        if (empty($purchaseInvoice)) {
            Flash::error('Purchase Invoice not found');

            return redirect(route('purchaseInvoices.index'));
        }

        $this->purchaseInvoiceRepository->delete($id);

        Flash::success('Purchase Invoice deleted successfully.');

        return redirect(route('purchaseInvoices.index'));
    }
    else{
        return Redirect::to('/login');
    }
}

    public function exportExcel(Request $request){
        if (Auth::check())
        {
        return Excel::download(new PurchaseExport($request['id']),'purchase.xlsx');
        }
        else{
            return Redirect::to('/login');
        }
    }
}
