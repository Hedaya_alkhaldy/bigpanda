<?php

namespace App\Http\Controllers;

use App\Exports\PurchaseReportExport;
use App\Models\Item;
use App\Models\PurchaseInvoice;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use PDF;

class PurchaseReportController extends Controller
{
    public function index()
    {
        if (Auth::check()) {

        return view('reports.purchase.index');
    }else{
        return Redirect::to('/login');
    }
    }
    public function show(Request $request)
    {
        if (Auth::check()) {

        $from = $request['from'];
        $to = $request['to'];
        $purchaseInvoices = PurchaseInvoice::whereBetween('invoice_id', array($from, $to))->with('items')->get();
        $items = Item::all();
        return view('reports.purchase.show')->with([
            'from' => $from, 'to' => $to, 'purchaseInvoices' => $purchaseInvoices,
            'items' => $items
        ]);
    }else{
        return Redirect::to('/login');
    }
    }
    public function exportExcel(Request $request)
    {
        if (Auth::check()) {

        return Excel::download(new PurchaseReportExport($request['from'], $request['to']), 'Purchase Report ' . $request['from'] . '-' . $request['to'] . '.xlsx');
    }else{
        return Redirect::to('/login');
    }
    }
}
