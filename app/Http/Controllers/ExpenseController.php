<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateExpenseRequest;
use App\Http\Requests\UpdateExpenseRequest;
use App\Repositories\ExpenseRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Flash;
use Response;

class ExpenseController extends AppBaseController
{
    /** @var  ExpenseRepository */
    private $expenseRepository;

    public function __construct(ExpenseRepository $expenseRepo)
    {
        $this->expenseRepository = $expenseRepo;
    }

    /**
     * Display a listing of the Expense.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('expenses.date');
    }

    /**
     * Show the form for creating a new Expense.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $mytime = Carbon::now();
        $date = $mytime->toDateTimeString() ;
        $date = date('Y-m-d');
        return view('expenses.create')->with(['expense'=> null,'date'=>$date,'from' => $request['from'],
        'to' => $request['to']]);
    }

    /**
     * Store a newly created Expense in storage.
     *
     * @param CreateExpenseRequest $request
     *
     * @return Response
     */
    public function store(CreateExpenseRequest $request)
    {
        $input = $request->all();
        $arraySize = count($request['reasons']);
        $reasons = $request['reasons'];
        $amount = $request['amount'];
        $notes=$request['notes'];
        $expense_date = $request['expense_date'];

        for ($i = 0; $i < $arraySize; $i++) {
            Expense::create(
                 [
                    'reason' => $reasons[$i],
                    'amount' => $amount[$i],
                    'notes' => $notes[$i],
                    'expense_date' => $expense_date

                ]
            );

        }
        Flash::success('Expense saved successfully.');

        if($request['from']!= null && $request['to']!=null){
            $expenses = Expense::whereBetween('expense_date', array($request['from'],
            $request['to']))->get();

            return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
            'to' => $request['to']]);
        }else{
            $expenses = Expense::all();

            return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
            'to' => $request['to']]);
        }

      }



    /**
     * Display the specified Expense.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $expense = $this->expenseRepository->find($id);

        if (empty($expense)) {
            Flash::error('Expense not found');

            return redirect(route('expenses.index'));
        }

        return view('expenses.show')->with(['expense'=>$expense,'from' => $request['from'],
        'to' => $request['to']]);
    }


    /**
     * Show the form for editing the specified Expense.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $expense = $this->expenseRepository->find($id);

        if (empty($expense)) {
            Flash::error('Expense not found');

            return redirect(route('expenses.index'));
        }

        return view('expenses.edit')->with(['expense'=> $expense,'date' => $expense->expense_date,'from' => $request['from'],
        'to' => $request['to']]);
    }

    /**
     * Update the specified Expense in storage.
     *
     * @param int $id
     * @param UpdateExpenseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExpenseRequest $request)
    {

        $arraySize = count($request['reasons']);
        $reasons = $request['reasons'];
        $amount = $request['amount'];
        $notes = $request['notes'];
        $expense_date = $request['expense_date'];
        $expense = Expense::where('id',$id)->first();

        if($arraySize == 1){
            $expense ->update([
                                'reason' => $reasons[0],
                                 'amount' => $amount[0],
                                 'notes' => $notes[0],
                                'expense_date' => $expense_date
                                 ]);

        }
        else{
            $expense ->update([
                'reason' => $reasons[0],
                 'amount' => $amount[0],
                 'notes' => $notes[0],
                'expense_date' => $expense_date
                 ]);

            for ($i = 1; $i < $arraySize; $i++) {
                Expense::create(
                     [
                        'reason' => $reasons[$i],
                        'amount' => $amount[$i],
                        'notes' => $notes[$i],
                        'expense_date' => $expense_date

                    ]
                );

            }


        }
        Flash::success('Expense updated successfully.');

        $expenses = Expense::whereBetween('expense_date', array($request['from'],
        $request['to']))->get();

        return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
        'to' => $request['to']]);

}

    /**
     * Remove the specified Expense from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $expense = $this->expenseRepository->find($id);

        if (empty($expense)) {
            Flash::error('Expense not found');

            return redirect(route('expenses.index'));
        }

        $this->expenseRepository->delete($id);

        Flash::success('Expense deleted successfully.');

        if($request['from']!= null && $request['to']!=null){
            $expenses = Expense::whereBetween('expense_date', array($request['from'],
            $request['to']))->get();

            return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
            'to' => $request['to']]);
        }else{
            $expenses = Expense::all();

            return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
            'to' => $request['to']]);
        }

        }
    /**
     * Enter a date of the Expense.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function expenseByDate(Request $request)
    {
        // dd($request->all());

        if (Auth::check()) {

            if($request['from']!= null && $request['to']!=null){
                $expenses = Expense::whereBetween('expense_date', array($request['from'],
                $request['to']))->get();

                return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
                'to' => $request['to']]);
            }else{
                $expenses = Expense::all();

                return view('expenses.index')->with(['expenses'=> $expenses,'from' => $request['from'],
                'to' => $request['to']]);
            }




        }else{
            return Redirect::to('/login');
        }
    }
}
