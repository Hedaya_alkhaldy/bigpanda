<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\SalesInvoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class SalesReportController extends Controller
{

    public function index()
    {
        if (Auth::check()) {

        return view('reports.sales.index');
    }else{
        return Redirect::to('/login');
    }

    }

    public function show(Request $request)
    {
        if (Auth::check()) {

        $salesInvoices = SalesInvoice::with('items')->whereBetween('invoice_id', array($request['from'],
        $request['to']))->get();
        $items = Item::all();

        return view('reports.sales.show')
        ->with(['salesInvoices' => $salesInvoices ,'items' => $items, 'to'=>$request['to'], 'from'=>$request['from']]);
    }else{
        return Redirect::to('/login');
    }
    }
}
