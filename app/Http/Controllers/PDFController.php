<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Order;
use App\Models\PurchaseInvoice;
use App\Models\Reservations;
use App\Models\SalesInvoice;
use App\Models\Screen;
use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    public function salesPDF(Request $request)
    {
        $salesInvoices = SalesInvoice::where('id', $request['id'])->with('items')->get();
        $items = Item::all();
        $date = '';

        foreach ($salesInvoices as $salesInvoice) {
            $date = $salesInvoice->invoice_id;
        }
        view()->share(['salesInvoices' => $salesInvoices, 'items' => $items, 'date' => $date]);
        $pdf = PDF::loadView('sales_invoices.pdf', $salesInvoices);

        // download PDF file with download method
        return $pdf->download('sales.pdf');
    }

    public function purchasePDF(Request $request)
    {
        $purchaseInvoices = PurchaseInvoice::where('id', $request['id'])->with('items')->get();
        $items = Item::all();

        view()->share(['purchaseInvoices' => $purchaseInvoices, 'items' => $items]);
        $pdf = PDF::loadView('purchase_invoices.pdf', $purchaseInvoices);

        // download PDF file with download method
        return $pdf->download('purchase.pdf');
    }
    public function allSalesPDF(Request $request)
    {
        $salesInvoices = SalesInvoice::with('items')->whereBetween('invoice_id', array(
            $request['from'],
            $request['to']
        ))->get();
        $items = Item::all();
        view()->share([
            'salesInvoices' => $salesInvoices, 'items' => $items,
            'from' => $request['from'], 'to' => $request['to']
        ]);
        $pdf = PDF::loadView('reports.sales.pdf', $salesInvoices);

        // download PDF file with download method
        return $pdf->download('Sales '. $request['from'] . '-' . $request['to'] . '.pdf');
    }
    public function purchaseReportPDF(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];
        $purchaseInvoices = PurchaseInvoice::whereBetween('invoice_id', array($from, $to))->with('items')->get();
        $items = Item::all();
        view()->share(['from' => $from, 'to' => $to, 'purchaseInvoices' => $purchaseInvoices, 'items' => $items]);
        $pdf = PDF::loadView('reports.purchase.pdf_table');

        // download PDF file with download method
        return $pdf->download('Purchase '. $request['from'] . '-' . $request['to'] . '.pdf');
    }

    public function reservationsPDF(Request $request)
    {

        $reservations = Reservations::where('id',$request['id'])->with('screens')->first();
        $screens = Screen::pluck('screen_name', 'id');
        $order = Order::where('reservation_id',$reservations->id)->first();
        $salesInvoice = SalesInvoice::where('id',$order->sale_invoice_id)->with('items')->first();
        $items = Item::all();



        view()->share(['reservations'=> $reservations , 'salesInvoice' => $salesInvoice ,'screens' => $screens,'items' => $items]);
        $pdf = PDF::loadView('reservations.pdf', $salesInvoice);

        // download PDF file with download method
        return $pdf->download('sales.pdf');
    }

}
