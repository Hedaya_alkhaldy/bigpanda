<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Reservations;
use App\Models\Screen;
use Illuminate\Http\Request;
use Carbon\Carbon;


class ScreenController extends Controller
{

 public function getStatus($screen){

  $status = Screen::select('screen_status_id')->where('screen_name', $screen)->first();
  if($status->screen_status_id == 1){
    return[
        'status' =>1
    ] ;

  }else
  {
    return[
        'status'=>0
    ];

  }

 }

 public function getReservationTime($screen){

    $screen = Screen::where('screen_name', $screen)->first();
    $screen_id =$screen->id;
    $reservation = Reservations::select('reservation_time_from','reservation_time_to')->where('screen_id', $screen_id)->first();
    [$hours, $minutes] = explode(':', $reservation->reservation_time_from);
    $reservation_time_from =  (int)$hours * 60 + (int)$minutes;
    [$hours, $minutes] = explode(':', $reservation->reservation_time_to);
    $hours = $hours == 00? 12 :$hours;
    $reservation_time_to =  (int)$hours * 60 + (int)$minutes;

    return [
        'time'=>$reservation_time_to -$reservation_time_from,
    ];


   }


 public function checkReservationTime($screen){

    $screen = Screen::where('screen_name', $screen)->first();
    $screen_id =$screen->id;
    $reservation = Reservations::select('reservation_time_to')->where('screen_id', $screen_id)->first();

    $reservation_time_to = $reservation->reservation_time_to;
    $curentDate = Carbon::now()->format('H:i:s');

    $date = $curentDate > $reservation_time_to ?'reservation end': 'reservation continue';




    return [
        'date'=>$date,
    ];


   }



}
