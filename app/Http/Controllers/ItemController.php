<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Repositories\ItemRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\PriceItemRequest;
use App\Http\Resources\PriceItemResource;
use Illuminate\Validation\Rule;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Flash;
use Response;



class ItemController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (Auth::check()) {

        $items = $this->itemRepository->all();

        return view('items.index')->with('items', $items);
        }else{
               return Redirect::to('/login');
           }
    }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::check()) {

        $items = Item::all();
        $formType = 'createType';
        if($items->isEmpty()){

            $numberOfItem = 0;

            return view('items.create')->with(['numberOfItem'=> $numberOfItem , 'formType'=> $formType]);

        }else{

            $sorted = $items->sortBy('item_number')->last();
            $numberOfItem = $sorted->item_number;
            return view('items.create')->with(['numberOfItem'=> $numberOfItem , 'formType'=> $formType]);

        }
    }else{
        return Redirect::to('/login');
    }

    }

    /**
     * Store a newly created Item in storage.
     *
     * @param CreateItemRequest $request
     *
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {
        if (Auth::check()) {

        $input = $request->all();
        //dd($input);

        $item = Item::create([
            'item_number'=> $input['item_number'],
            'name' => $input['name'],
            'sale_price' => $input['sale_price'],


        ]);

        Flash::success('Item saved successfully.');

        return redirect(route('items.index'));
    }else{
        return Redirect::to('/login');
    }
    }

    /**
     * Display the specified Item.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (Auth::check()) {

        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        return view('items.show')->with('item', $item);
    }else{
        return Redirect::to('/login');
    }
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::check()) {

        $item = $this->itemRepository->find($id);
        $formType = 'editType';

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        return view('items.edit')->with(['item'=> $item,'formType' =>$formType]);
    }else{
        return Redirect::to('/login');
    }
    }

    /**
     * Update the specified Item in storage.
     *
     * @param int $id
     * @param UpdateItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemRequest $request)
    {
        if (Auth::check()) {

        $item = $this->itemRepository->find($id);
        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        $request->validate([
        'item_number' => [
            'required',
            Rule::unique('items')->ignore($item->id),
        ],    ]);

        $item = $this->itemRepository->update($request->all(), $id);

        Flash::success('Item updated successfully.');

        return redirect(route('items.index'));
    }else{
        return Redirect::to('/login');
    }
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {

        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        $this->itemRepository->delete($id);

        Flash::success('Item deleted successfully.');

        return redirect(route('items.index'));
    }else{
        return Redirect::to('/login');
    }
}

     /**
     * Remove the specified Item from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function getItemPrice($id)
    {
        if (Auth::check()) {
        $allItem = Item::where('id',$id)->with('purchaseInvoices')->first();

        $item = PriceItemResource::make($allItem);
        return $item;



    }else{
        return Redirect::to('/login');
    }
    }
}
