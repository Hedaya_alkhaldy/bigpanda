<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateScreenRequest;
use App\Http\Requests\UpdateScreenRequest;
use App\Repositories\ScreenRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Screen;
use App\Models\ScreenStatus;
use Illuminate\Http\Request;
use Flash;
use Response;

class ScreenController extends AppBaseController
{
    /** @var  ScreenRepository */
    private $screenRepository;

    public function __construct(ScreenRepository $screenRepo)
    {
        $this->screenRepository = $screenRepo;
    }

    /**
     * Display a listing of the Screen.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $screens = Screen::with('screenStatus')->get();

        return view('screens.index')
            ->with('screens', $screens);
    }

    /**
     * Show the form for creating a new Screen.
     *
     * @return Response
     */
    public function create()
    {
        $screenStauts = ScreenStatus::pluck('name', 'id');
        return view('screens.create')->with('screenStauts',$screenStauts);
    }

    /**
     * Store a newly created Screen in storage.
     *
     * @param CreateScreenRequest $request
     *
     * @return Response
     */
    public function store(CreateScreenRequest $request)
    {
        $input = $request->all();


        $screen = $this->screenRepository->create($input);

        Flash::success('Screen saved successfully.');

        return redirect(route('screens.index'));
    }

    /**
     * Display the specified Screen.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $screen = Screen::where('id',$id)->with('screenStatus')->first();

        if (empty($screen)) {
            Flash::error('Screen not found');

            return redirect(route('screens.index'));
        }

        return view('screens.show')->with('screen', $screen);
    }

    /**
     * Show the form for editing the specified Screen.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $screen = $this->screenRepository->find($id);
        $screenStauts = ScreenStatus::pluck('name', 'id');


        if (empty($screen)) {
            Flash::error('Screen not found');

            return redirect(route('screens.index'));
        }

        return view('screens.edit')->with(['screen'=> $screen, 'screenStauts'=> $screenStauts]);
    }

    /**
     * Update the specified Screen in storage.
     *
     * @param int $id
     * @param UpdateScreenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateScreenRequest $request)
    {
        $screen = $this->screenRepository->find($id);


        if (empty($screen)) {
            Flash::error('Screen not found');

            return redirect(route('screens.index'));
        }

        $screen = $this->screenRepository->update($request->all(), $id);

        Flash::success('Screen updated successfully.');

        return redirect(route('screens.index'));
    }

    /**
     * Remove the specified Screen from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $screen = $this->screenRepository->find($id);

        if (empty($screen)) {
            Flash::error('Screen not found');

            return redirect(route('screens.index'));
        }

        $this->screenRepository->delete($id);

        Flash::success('Screen deleted successfully.');

        return redirect(route('screens.index'));
    }
}
