<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Screen;
use Illuminate\Http\Request;

class UpdateScreenRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            // 'screen_name' => 'required|unique:screens,screen_name,' . $request->id,
            'screen_name' => 'required',
            'screen_status_id' => 'required',
        ];
    }
}
