<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Reservations
 * @package App\Models
 * @version March 30, 2022, 11:28 am UTC
 *
 * @property integer $screen_id
 * @property string $reservation_time_from
 * @property string $reservation_time_to
 * @property string $reservation_date
 */
class Reservations extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'reservations';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'screen_id',
        'reservation_time_from',
        'reservation_time_to',
        'reservation_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'screen_id' => 'integer',
        'reservation_time_from' => 'string',
        'reservation_time_to' => 'string',
        'reservation_date' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'screen_id' => 'required',
        'reservation_time_from' => 'required',
        'reservation_time_to' => 'required',
    ];

       /**
         * Get the user that owns the Screen
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function screens()

        {
            return $this->belongsTo(Screen::class,'screen_id');

        }


}
