<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Expense
 * @package App\Models
 * @version April 5, 2022, 9:24 am UTC
 *
 * @property string $reason
 * @property integer $amount
 * @property string $expense_date
 * @property string $notes
 */
class Expense extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'expenses';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'reason',
        'amount',
        'expense_date',
        'notes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'reason' => 'string',
        'amount' => 'integer',
        'expense_date' => 'string',
        'notes' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'reasons' => 'required',
        'amount' => 'required',
        'expense_date' => 'required'
    ];


}
