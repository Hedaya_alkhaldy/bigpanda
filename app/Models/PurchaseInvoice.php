<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PurchaseInvoice
 * @package App\Models
 * @version January 5, 2022, 10:47 am UTC
 *
 * @property string $date
 * @property number $provider_id
 * @property string $type
 */
class PurchaseInvoice extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'purchase_invoices';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'invoice_id',
        'provider_id',
        'type',
        'quantity',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'invoice_id' => 'date',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'provider_id' => 'required',
        'type' => 'required'
    ];

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    public function items()
    {
        return $this->belongsToMany(Item::class, 'item_purchase_invoice', 'invoice_id','item_id')
            ->withPivot('quantity', 'purchasing_price', 'total' , 'id');
    }

     protected static function boot()
    {
        parent::boot();
        static::deleting(function ($purchaseInvoice){
            $purchaseInvoice->items()->detach();

        });
    }


}
