<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Screen
 * @package App\Models
 * @version March 30, 2022, 11:11 am UTC
 *
 * @property integer $screen_id
 * @property integer $screen_status_id
 * @property string $notes
 */
class Screen extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'screens';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'screen_name',
        'screen_status_id',
        'notes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'screen_name' => 'string',
        'screen_status_id' => 'integer',
        'notes' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'screen_name' => 'required|unique:screens',
        'screen_status_id' => 'required',
    ];


        /**
         * Get the user that owns the Screen
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function screenStatus()

        {
            return $this->belongsTo(ScreenStatus::class);

        }





}
