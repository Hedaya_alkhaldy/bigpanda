<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Item
 * @package App\Models
 * @version January 5, 2022, 10:39 am UTC
 *
 * @property string $name
 */
class Item extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'items';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'item_number',
        'sale_price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'sale_price' => 'float',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'sale_price' => 'required',
        'item_number' => 'required|unique:items'
    ];

      /**
     * Validation rules
     *
     * @var array
     */
    public static $editRules = [
        'name' => 'required',
    ];


    public function purchaseInvoices()
    {
        return $this->belongsToMany(PurchaseInvoice::class, 'item_purchase_invoice', 'item_id', 'invoice_id')
            ->withPivot('quantity', 'purchasing_price', 'total' , 'id');
    }

    public function salesInvoices()
    {
        return $this->belongsToMany(SalesInvoice::class, 'item_sales_invoice', 'item_id', 'invoice_id')
            ->withPivot('quantity', 'sale_price', 'total');
    }


}
