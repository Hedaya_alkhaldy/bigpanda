<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ScreenStatus
 * @package App\Models
 * @version March 30, 2022, 11:11 am UTC
 *
 * @property string $name
 */
class ScreenStatus extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'screen_statuses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
