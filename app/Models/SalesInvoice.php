<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use DB;

/**
 * Class SalesInvoice
 * @package App\Models
 * @version January 5, 2022, 10:52 am UTC
 *
 * @property string $id
 */
class SalesInvoice extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'sales_invoices';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'invoice_id',
        'quantity',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'invoice_id' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'id' => 'required'
    ];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'item_sales_invoice', 'invoice_id' , 'item_id')
            ->withPivot('quantity', 'sale_price', 'total','id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($saleInvoice){
            $saleInvoice->items()->detach();

        });
    }

    public static function getEmployee($id){

        // $salesInvoice=SalesInvoice::select('invoice_id','quantity','total')->where('id',$id )
        // ->with('items')->get();
        // $salesInvoices = SalesInvoice::with('items')->select('invoice_id','quantity','total')->where('id',$id )->get();


        // dd($salesInvoices);
        return DB::table('inmuebles_personas')->get();


        return $salesInvoice;
    }

}
