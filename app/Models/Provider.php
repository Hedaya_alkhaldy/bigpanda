<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Provider
 * @package App\Models
 * @version January 5, 2022, 10:42 am UTC
 *
 * @property string $name
 */
class Provider extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'providers';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'phone' =>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'required|regex:/(0)[0-9]{9}/'
    ];

    public function purchaseInvoices()
    {
        return $this->hasMany(PurchaseInvoice::class);
    }


}
