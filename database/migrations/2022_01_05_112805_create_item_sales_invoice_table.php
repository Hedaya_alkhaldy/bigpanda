<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemSalesInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_sales_invoice', function (Blueprint $table) {
            $table->id();
            $table->double('quantity')->default(0);
            $table->double('sale_price')->default(0.0);;
            $table->double('total');
            $table->foreignId('item_id')->references('id')->on('items');
            $table->foreignId('invoice_id')->references('id')->on('sales_invoices');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_sales_invoice');
    }
}
