<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemPurchaseInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_purchase_invoice', function (Blueprint $table) {
            $table->id();
            $table->double('quantity')->default(0);
            $table->double('purchasing_price')->default(0.0);;
            $table->double('total');
            $table->foreignId('item_id')->references('id')->on('items');
            $table->foreignId('invoice_id')->references('id')->on('purchase_invoices');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_purchase_invoice');
    }
}
