<?php

namespace Database\Seeders;

use App\Models\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'ببسي' , 'item_number'=>'1'],
            ['name'=>'شسبس' , 'item_number'=>'2'],
            ['name'=>'كولا' , 'item_number'=>'3'],
            ['name'=>'قهوة' , 'item_number'=>'4'],
            ['name'=>'شاي' , 'item_number'=>'5'],
            ['name'=>'نسكافيه' , 'item_number'=>'6'],
            ['name'=>'موكا' , 'item_number'=>'7'],
            ['name'=>'لاتيه' , 'item_number'=>'8'],
            ['name'=>'شوكلاته' , 'item_number'=>'9'],
            ['name'=>'دونتس' , 'item_number'=>'10'],
        ];

        Item::insert($data);

    }
}
