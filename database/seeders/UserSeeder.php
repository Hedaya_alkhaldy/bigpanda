<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' =>'Admin',
            'email' => 'admin@BigPanda.com',
            'password' => Hash::make('12345678'),],
        ];

        User::insert($data);
    }
}
