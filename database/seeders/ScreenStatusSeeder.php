<?php

namespace Database\Seeders;

use App\Models\ScreenStatus;
use Illuminate\Database\Seeder;

class ScreenStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'Available'],
            ['name'=>'Busy'],
            ['name'=>'Disabled'],
            ['name'=>'Reserved'],
        ];

        ScreenStatus::insert($data);
    }
}
